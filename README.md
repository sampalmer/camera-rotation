# Camera Rotation

### Problem
Two problems:
* Sideways Mode requires the user to manually start it and tell it which side they're lying on
* The stock auto-rotate algorithm sometimes gets it wrong, such as when you take the device out of your pocket

### Solution
Use the device's front-facing camera to check which orientation the user is in. Make sure a manual override is available in case the app has trouble seeing the user.

One problem with this approach is it won't work in the dark without special camera hardware or a soft flash effect.

### When to run the camera

* When the user unlocks the phone (because they might already be lying down)
* When the device orientation changes (because the user might be repositioning). But only if the phone's physical location also changes (because if it doesn't then they're probably switching screen orientations).

### When not to run the camera

* When the user has just opened an app, since it might be the camera app
* When the device is in motion, since the user might be in the middle of positioning it or getting it out of his/her pocket

Note that the camera2 API allows foreground apps to "steal" the camera from background apps, so hopefully this will reduce impact on other camera apps.

Also, the camera2 API has callbacks for camera availability.

### Requirements

- [ ] Tolerate low light conditions
    - [ ] Set the preview FPS range to have the range that has the greatest difference between min & max values. This allows the camera to adjust exposure as needed.
    - [ ] Turn auto exposure on
    - [ ] When needed, make a 'soft flash' by turning up the device screen brightness and making the whole screen white
    - [ ] Max out ISO setting if it helps
    - [ ] Max out shutter speed if it helps
    - [ ] Max out exposure compensation if it helps
    - [ ] Consider post-processing the image to make it easier for the face detection (eg brightness increase)
- [ ] Minimise impact on other apps that also use the camera
- [ ] Make sure battery usage is reasonable
- [ ] Consider using [dynamic delivery](https://developer.android.com/studio/projects/dynamic-delivery) to minimise impact of the app size increase
- [ ] Suppress the "*Sideways Mode won't run unless you update Google Play services*" notification
- [ ] Provide a manual fallback for when the user's orientation cannot be determined (eg at night)
- [ ] Change metering mode to not just use the centre since the user might not be centred in the frame and there could be something bright in the background

---

### Libraries

Library | Minimum light level | Partial face support | APK size overhead | Install size overhead | Average duration | Accuracy
--------|---------------------|----------------------|------------------|-------------|---|---
Google Mobile Vision | Typical | No | ? | ? | 1000ms | Good
Firebase ML Kit | Low | Yes | 10 MB | 15 MB | 200ms | Poor (lots of false positives)
Microsoft Cognitive Services (no offline support) | ? | ? | ? | ?
