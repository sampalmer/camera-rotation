package samuelpalmer.cameraautorotation;

import org.junit.Test;

import samuelpalmer.cameraautorotation.utils.AngleUtils;

import static org.junit.Assert.*;

public class NormaliseAngleTests {

    @Test
    public void convertsNegativeToPositive() {
        assertEquals(270, AngleUtils.normaliseAngle(-90), .01);
    }

    @Test
    public void preservesAlreadyNormalisedAngle() {
        assertEquals(90, AngleUtils.normaliseAngle(90), .01);
    }

    @Test
    public void wrapsPast360() {
        assertEquals(0, AngleUtils.normaliseAngle(360), .01);
        assertEquals(90, AngleUtils.normaliseAngle(450), .01);
    }

    @Test
    public void wrapsNegativeAnglesPast360() {
        assertEquals(0, AngleUtils.normaliseAngle(-360), .01);
        assertEquals(270, AngleUtils.normaliseAngle(-450), .01);
    }

}
