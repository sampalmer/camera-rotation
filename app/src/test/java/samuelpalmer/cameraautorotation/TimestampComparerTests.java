package samuelpalmer.cameraautorotation;

import org.hamcrest.Matchers;
import org.junit.Test;

import samuelpalmer.cameraautorotation.utils.TimestampComparer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class TimestampComparerTests {

    @Test
    public void treatsEqualsAsEqual() {
        assertEquals(0, TimestampComparer.compare(Long.MIN_VALUE, Long.MIN_VALUE));
        assertEquals(0, TimestampComparer.compare(Long.MAX_VALUE, Long.MAX_VALUE));
        assertEquals(0, TimestampComparer.compare(0L, 0L));
    }

    @Test
    public void treatsSmallerAsSmaller() {
        assertThat(TimestampComparer.compare(Long.MIN_VALUE, Long.MIN_VALUE + 1), Matchers.lessThan(0));
        assertThat(TimestampComparer.compare(0, 1), Matchers.lessThan(0));
        assertThat(TimestampComparer.compare(Long.MAX_VALUE - 1, Long.MAX_VALUE), Matchers.lessThan(0));
    }

    @Test
    public void treatsLargerAsLarger() {
        assertThat(TimestampComparer.compare(Long.MIN_VALUE + 1, Long.MIN_VALUE), Matchers.greaterThan(0));
        assertThat(TimestampComparer.compare(1, 0), Matchers.greaterThan(0));
        assertThat(TimestampComparer.compare(Long.MAX_VALUE, Long.MAX_VALUE - 1), Matchers.greaterThan(0));
    }

    @Test
    public void treatsOverflowAsGreater() {
        // If a timestamp exceeds MAX_VALUE, then it will wrap around. But it is still considered greater!

        assertThat(TimestampComparer.compare(Long.MIN_VALUE, Long.MAX_VALUE), Matchers.greaterThan(0));
        assertThat(TimestampComparer.compare(Long.MAX_VALUE, Long.MIN_VALUE), Matchers.lessThan(0));
    }

    @Test
    public void noIntOverflow() {
        // Need to make sure that if the difference exceeds Integer.MAX_VALUE, that the result is still correct

        assertThat(TimestampComparer.compare(Long.MAX_VALUE, 0), Matchers.greaterThan(0));
    }

}
