package samuelpalmer.cameraautorotation;

import org.junit.Test;

import samuelpalmer.cameraautorotation.utils.AngleUtils;

import static org.junit.Assert.*;

public class AngleDifferenceTests {

    @Test
    public void convertsNegativeToPositive() {
        assertEquals(90, AngleUtils.normaliseAngleDifference(90 - 180), .01);
    }

    @Test
    public void preservesAlreadyNormalisedAngle() {
        assertEquals(90, AngleUtils.normaliseAngleDifference(180 - 90), .01);
    }

    @Test
    public void wrapsPast180() {
        assertEquals(180, AngleUtils.normaliseAngleDifference(180 - 0), .01);
        assertEquals(90, AngleUtils.normaliseAngleDifference(360 - 90), .01);
    }

    @Test
    public void wrapsNegativeAnglesPast180() {
        assertEquals(180, AngleUtils.normaliseAngleDifference(0 - 180), .01);
        assertEquals(90, AngleUtils.normaliseAngleDifference(90 - 360), .01);
    }

    @Test
    public void z() {
        assertEquals(2, AngleUtils.normaliseAngleDifference(358 - 0), .01);
    }

    @Test
    public void wrapsPast360() {
        assertEquals(0, AngleUtils.normaliseAngleDifference(720 - 0), .01);
    }
}
