package samuelpalmer.cameraautorotation.service;

import android.app.AppOpsManager;
import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.Surface;
import android.view.WindowManager;

import samuelpalmer.cameraautorotation.utils.AppOpMonitor;

public class ScreenOrientationController {
    
    private static final String TAG = ScreenOrientationController.class.getSimpleName();
    private final WindowManager windowManager;

    public static boolean hasPermissionToStart(Context context) {
        return makePermissionMonitor(context).hasPermission();
    }

    public static AppOpMonitor makePermissionMonitor(Context context) {
        return new AppOpMonitor(context, AppOpsManager.OPSTR_WRITE_SETTINGS);
    }

    private final Context context;
    private final ContentResolver contentResolver;
    private final AppOpMonitor systemSettingPermissionMonitor;

    private int lastProposed = -1;
    private boolean started;

    public ScreenOrientationController(Context context) {
        this.context = context;
        windowManager = context.getSystemService(WindowManager.class);
        contentResolver = context.getContentResolver();

        systemSettingPermissionMonitor = makePermissionMonitor(context);
    }

    public void start() {
        if (started)
            throw new IllegalStateException("Already started");

        started = true;
        lastProposed = Settings.System.getInt(contentResolver, Settings.System.USER_ROTATION, Surface.ROTATION_0);

        if (Settings.System.getInt(contentResolver, Settings.System.ACCELEROMETER_ROTATION, 1) == 1) {
            int realOrientation = windowManager.getDefaultDisplay().getRotation();
            propose(realOrientation);
            
            setAndHandleErrors(Settings.System.ACCELEROMETER_ROTATION, 0);
            propose(realOrientation); //Just in case disabling system rotation reset this.
        }
        
        contentResolver.registerContentObserver(Settings.System.getUriFor(Settings.System.USER_ROTATION), false, onUserRotationChanged);
        systemSettingPermissionMonitor.subscribe(systemSettingPermissionChanged);
    }

    public void stop() {
        if (!started)
            throw new IllegalStateException("Already stopped");

        systemSettingPermissionMonitor.unsubscribe(systemSettingPermissionChanged);
        contentResolver.unregisterContentObserver(onUserRotationChanged);
        setAndHandleErrors(Settings.System.ACCELEROMETER_ROTATION, 1);
        propose(Surface.ROTATION_0);
        lastProposed = -1;

        started = false;
    }

    public void propose(int value) {
        Log.i(TAG, "Proposing screen orientation: " + value);
        
        boolean worked = setAndHandleErrors(Settings.System.USER_ROTATION, value);
        if (worked)
            lastProposed = value;
    }

    public int getLastProposed() {
        return lastProposed;
    }

    private boolean setAndHandleErrors(String name, int newValue) {
        if (!validateSystemSettingsPermission())
            return false;

        boolean worked = Settings.System.putInt(contentResolver, name, newValue);
        if (!worked) {
            // TODO: Present this error to the user since it's not the app's fault
            throw new RuntimeException("Failed to write system settings");
        }
        
        return true;
    }

    private final ContentObserver onUserRotationChanged = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            
            if (started && lastProposed != -1) {
                int newUserRotation = Settings.System.getInt(contentResolver, Settings.System.USER_ROTATION, Surface.ROTATION_0);
                if (newUserRotation != lastProposed) {
                    /*
                     * Android P sometimes changes the value of USER_ROTATION to match the current screen orientation.
                     * This can be problematic, since the current screen orientation might be the result of an open app's
                     * orientation preferences. For example, when the launcher is open, the screen orientation is usually portrait.
                     *
                     * That breaks this app, so we'll just have to fight with the OS and change it back.
                     */
                    Log.i(TAG, "Something proposed orientation " + newUserRotation + ". Reverting to " + lastProposed + ".");
                    setAndHandleErrors(Settings.System.USER_ROTATION, lastProposed);
                }
            }
        }
    };

    private final AppOpMonitor.Listener systemSettingPermissionChanged = op -> validateSystemSettingsPermission();

    /**
     * Raises an error if system settings permission is missing.
     * @return Whether the permission is granted
     */
    private boolean validateSystemSettingsPermission() {
        if (!hasPermissionToStart(context)) {
            // TODO: Report the issue to the user
            return false;
        }
        else
            return true;
    }

}
