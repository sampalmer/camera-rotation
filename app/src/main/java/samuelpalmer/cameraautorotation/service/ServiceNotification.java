package samuelpalmer.cameraautorotation.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;

import samuelpalmer.cameraautorotation.NotificationChannels;
import samuelpalmer.cameraautorotation.NotificationIds;
import samuelpalmer.cameraautorotation.PendingIntentIds;
import samuelpalmer.cameraautorotation.R;
import samuelpalmer.cameraautorotation.ui.MainActivity;

class ServiceNotification implements DefaultLifecycleObserver {

    private static final String TAG = ServiceNotification.class.getSimpleName();

    private final Service service;

    public ServiceNotification(Service service) {
        this.service = service;
    }

    @Override
    public void onStart(@NonNull LifecycleOwner owner) {
        PendingIntent mainActivityIntent = PendingIntent.getActivity(
                service,
                PendingIntentIds.MAIN_NOTIFICATION_CLICK,
                new Intent(service, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK),
                PendingIntentIds.PENDING_INTENT_FLAGS);

        Notification notification = new NotificationCompat.Builder(service, NotificationChannels.ID_SERVICE)
                .setSmallIcon(R.drawable.camera)
                .setCategory(NotificationCompat.CATEGORY_SERVICE) // Using "service" category since this notification is more of an unwanted thing that the user can safely ignore
                .setContentTitle("Monitoring camera")
                .setContentIntent(mainActivityIntent)
                .setLocalOnly(true)
                .setPriority(NotificationCompat.PRIORITY_MIN)
                .setShowWhen(false)
                .setVisibility(NotificationCompat.VISIBILITY_SECRET) // This is to hide the notification from the lock screen since it's not relevant there
                .setOnlyAlertOnce(true) // Don't want to alert the user each time we update the state of the service notification
                .build();

        Log.i(TAG, "Starting foreground for service " + service.getClass().getSimpleName());
        service.startForeground(NotificationIds.SERVICE, notification);
    }

    @Override
    public void onStop(@NonNull LifecycleOwner owner) {
        service.stopForeground(true);
    }

}
