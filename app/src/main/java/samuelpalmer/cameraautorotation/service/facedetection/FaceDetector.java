package samuelpalmer.cameraautorotation.service.facedetection;

import android.media.Image;

public interface FaceDetector {

    FaceDetectionRequest detectInImage(Image rawImage, int rotationCompensationClockwiseDegrees, FaceDetectionListener listener);
    void setPredictedUserAngle(float predictedUserAngle);
    void close();

}
