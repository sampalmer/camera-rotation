package samuelpalmer.cameraautorotation.service.facedetection;

public interface FaceDetectionListener {
    /**
     * Called after detection completes. Note that this is <b>not</b> called if detection is cancelled.
     * The request is no longer valid after this point.
     */
    void onDetectionResult(FaceDetectionResult result);

    /**
     * Called after the request has been cancelled. The request is no longer valid after this point.
     */
    void onCancelled();
}
