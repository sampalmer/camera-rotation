package samuelpalmer.cameraautorotation.service.facedetection;

public interface FaceDetectionRequest {
    /**
     * Aborts the request
     * @throws IllegalStateException when the request has already completed or been cancelled
     */
    void cancel();
}
