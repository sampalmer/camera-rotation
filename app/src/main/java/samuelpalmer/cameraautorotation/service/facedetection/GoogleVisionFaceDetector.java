package samuelpalmer.cameraautorotation.service.facedetection;

import android.content.Context;
import android.media.Image;

public class GoogleVisionFaceDetector implements FaceDetector {

    private static final String TAG = GoogleVisionFaceDetector.class.getSimpleName();

    private boolean isRunning;
    private Context context;
    private com.google.android.gms.vision.face.FaceDetector faceDetector;
    private float predictedUserAngle;
    private FaceDetectionRequest request;

    public GoogleVisionFaceDetector(Context context) {
        isRunning = true;

        this.context = context;

        faceDetector = new
                com.google.android.gms.vision.face.FaceDetector.Builder(context)
                .setMinFaceSize(0.1f)
                .setProminentFaceOnly(true)
                .setTrackingEnabled(false) // Tracking doesn't seem to improve performance, so we might as well turn it off and re-use the detector for all 4 image orientations
                .build();

        if(!faceDetector.isOperational())
            throw new IllegalStateException("Google Mobile Vision face detection is not currently operational");
    }

    @Override
    public void close() {
        if (!isRunning)
            throw new IllegalStateException("Already closed");

        if (request != null) {
            request.cancel();
            request = null;
        }

        predictedUserAngle = 0;
        faceDetector.release();
        faceDetector = null;
        context = null;
        isRunning = false;
    }

    @Override
    public FaceDetectionRequest detectInImage(Image rawImage, int rotationCompensationClockwiseDegrees, FaceDetectionListener listener) {
        if (faceDetector == null)
            throw new IllegalStateException();

        request = new GoogleVisionFaceDetectionRequest(context, rawImage, predictedUserAngle, rotationCompensationClockwiseDegrees, faceDetector, new FaceDetectionListener() {
            @Override
            public void onDetectionResult(FaceDetectionResult result) {
                if (isRunning && request != null) {
                    request = null;
                    listener.onDetectionResult(result);
                }
            }

            @Override
            public void onCancelled() {
                if (isRunning && request != null) {
                    request = null;
                    listener.onCancelled();
                }
            }
        });

        return request;
    }

    @Override
    public void setPredictedUserAngle(float predictedUserAngle) {
        this.predictedUserAngle = predictedUserAngle;
    }

}
