package samuelpalmer.cameraautorotation.service.facedetection;

import android.media.Image;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

class CompositeFaceDetectionRequest implements FaceDetectionRequest {

    private boolean isRunning;
    private Collection<FaceDetector> detectors;
    private FaceDetectionListener parentListener;
    private HashMap<FaceDetector, FaceDetectionResult> detectionResults;
    private Set<FaceDetector> detectorsRemaining;
    private int rotationCompensationClockwiseDegrees;
    private Image rawImage;
    private FaceDetectionRequest componentRequest;

    public CompositeFaceDetectionRequest(Image rawImage, int rotationCompensationClockwiseDegrees, Collection<FaceDetector> detectors, FaceDetectionListener parentListener) {
        isRunning = true;
        this.detectors = detectors;
        this.parentListener = parentListener;
        detectionResults = new HashMap<>();
        this.rawImage = rawImage;
        this.rotationCompensationClockwiseDegrees = rotationCompensationClockwiseDegrees;
        detectorsRemaining = new HashSet<>(detectors);
        FaceDetector detector = detectorsRemaining.iterator().next();
        componentRequest = detector.detectInImage(rawImage, rotationCompensationClockwiseDegrees, new ComponentListener(detector));
    }

    @Override
    public void cancel() {
        if (!isRunning)
            throw new IllegalStateException("Already closed");

        FaceDetectionListener parentListenerBackup = this.parentListener;
        close();
        parentListenerBackup.onCancelled();
    }

    private void close() {
        if (!isRunning)
            throw new IllegalStateException("Already closed");

        if (componentRequest != null) {
            componentRequest.cancel();
            componentRequest = null;
        }

        detectionResults = null;
        detectorsRemaining = null;
        rotationCompensationClockwiseDegrees = 0;
        rawImage = null;
        parentListener = null;
        detectors = null;
        isRunning = false;
    }

    private class ComponentListener implements FaceDetectionListener {

        private final FaceDetector detector;

        public ComponentListener(FaceDetector detector) {
            this.detector = detector;
        }

        @Override
        public void onDetectionResult(FaceDetectionResult newResult) {
            if (isRunning) {
                detectionResults.put(detector, newResult);
                detectorsRemaining.remove(detector);

                boolean finished = detectionResults.size() == detectors.size();

                if (finished) {
                    FaceDetectionResult bestResult = null;
                    for (FaceDetectionResult result : detectionResults.values())
                        if (result.prominentFace != null) {
                            bestResult = result;
                            break;
                        }

                    componentRequest = null;

                    FaceDetectionListener parentListenerBackup = CompositeFaceDetectionRequest.this.parentListener;
                    close();
                    parentListenerBackup.onDetectionResult(bestResult);
                } else {
                    FaceDetector detector = detectorsRemaining.iterator().next();
                    componentRequest = detector.detectInImage(rawImage, rotationCompensationClockwiseDegrees, new ComponentListener(detector));
                }
            }
        }

        @Override
        public void onCancelled() {
            // This means we cancelled the component's detection request
        }

    }

}
