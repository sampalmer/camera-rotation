package samuelpalmer.cameraautorotation.service.facedetection;

import android.content.Context;
import android.media.Image;

import java.util.HashSet;
import java.util.Set;

public class CompositeFaceDetector implements FaceDetector {

    private Set<FaceDetector> components;
    private FaceDetectionRequest request;

    public CompositeFaceDetector(Context context) {
        components = new HashSet<>();
        components.add(new FirebaseMlFaceDetector());
        components.add(new GoogleVisionFaceDetector(context));
    }

    @Override
    public void close() {
        if (components == null)
            throw new IllegalStateException("Already closed");

        if (request != null) {
            request.cancel();
            request = null;
        }

        for (FaceDetector detector : components)
            detector.close();

        components = null;
    }

    @Override
    public FaceDetectionRequest detectInImage(Image rawImage, int rotationCompensationClockwiseDegrees, FaceDetectionListener parentListener) {
        if (components == null)
            throw new IllegalStateException("Detector is closed");

        if (request != null)
            throw new IllegalStateException("Detection is already running");

        request = new CompositeFaceDetectionRequest(rawImage, rotationCompensationClockwiseDegrees, components, new FaceDetectionListener() {
            @Override
            public void onDetectionResult(FaceDetectionResult result) {
                if (components != null && request != null) {
                    request = null;
                    parentListener.onDetectionResult(result);
                }
            }

            @Override
            public void onCancelled() {
                if (components != null && request != null) {
                    request = null;
                    parentListener.onCancelled();
                }
            }
        });

        return request;
    }

    @Override
    public void setPredictedUserAngle(float predictedUserAngle) {
        for (FaceDetector component : components)
            component.setPredictedUserAngle(predictedUserAngle);
    }

}
