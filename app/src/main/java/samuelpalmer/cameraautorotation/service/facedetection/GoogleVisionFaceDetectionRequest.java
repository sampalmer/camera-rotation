package samuelpalmer.cameraautorotation.service.facedetection;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.media.Image;
import android.os.Handler;
import android.util.Log;
import android.util.SparseArray;
import android.view.Surface;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;

import java.util.Arrays;

import samuelpalmer.cameraautorotation.utils.AngleUtils;
import samuelpalmer.cameraautorotation.utils.BitmapUtils;
import samuelpalmer.cameraautorotation.utils.Stopwatch;

class GoogleVisionFaceDetectionRequest implements FaceDetectionRequest {

    private static final String TAG = GoogleVisionFaceDetectionRequest.class.getSimpleName();

    private boolean isRunning;
    private Stopwatch stopwatch;
    private Handler handler;
    private FaceDetectionResult result;
    private FaceDetectionListener listener;

    public GoogleVisionFaceDetectionRequest(Context context, Image rawImage, final float lastFaceAngle, int rotationCompensationClockwiseDegrees, FaceDetector faceDetector, FaceDetectionListener listener) {
        this.listener = listener;
        isRunning = true;

        Bitmap bitmap = BitmapUtils.YUV_420_888_toRGBIntrinsics(context, rawImage);

        stopwatch = Stopwatch.startNew();

        Integer[] prioritisedOffsets = new Integer[] { 0, 90, 270, 180 };

        if (!Float.isNaN(lastFaceAngle))
            // We'll try using the offsets that are closest to the last known face angle to improve performance
            Arrays.sort(prioritisedOffsets, (offsetA, offsetB) -> {
                float offsetADifferenceFromLastFaceAngle = AngleUtils.normaliseAngleDifference(offsetA - lastFaceAngle);
                float offsetBDifferenceFromLastFaceAngle = AngleUtils.normaliseAngleDifference(offsetB - lastFaceAngle);
                return Float.compare(offsetADifferenceFromLastFaceAngle, offsetBDifferenceFromLastFaceAngle);
            });

        // The face detector only detects faces that are upright, so we need to keep rotating the image until we find a face.
        result = null;
        for (int offset : prioritisedOffsets) {
            int imageRotationCompensation = Surface.ROTATION_0 + ((rotationCompensationClockwiseDegrees + offset) % 360) / 90;

            Frame frame = new Frame.Builder()
                    .setBitmap(bitmap)
                    .setRotation(imageRotationCompensation)
                    .build();

            SparseArray<Face> faces = faceDetector.detect(frame);
            if (faces.size() > 0) {
                FaceDetectionFace resultProminentFace = null;
                FaceDetectionFace[] resultFaces = new FaceDetectionFace[faces.size()];

                for (int i = 0; i < faces.size(); ++i) {
                    Face face = faces.valueAt(i);
                    FaceDetectionFace resultFace = faceToResultFace(face, offset, rawImage, imageRotationCompensation);
                    resultFaces[i] = resultFace;

                    if (i == 0)
                        resultProminentFace = resultFace;
                }

                result = new FaceDetectionResult(resultProminentFace, resultFaces);

                break;
            }
        }

        if (result == null)
            result = new FaceDetectionResult();

        handler = new Handler();

        handler.post(sendResult);
    }

    @SuppressWarnings("SuspiciousNameCombination")
    private static FaceDetectionFace faceToResultFace(Face face, int offset, Image rawImage, int imageRotationCompensation) {
        Rect bounds = new Rect(
                Math.round(face.getPosition().x),
                Math.round(face.getPosition().y),
                Math.round(face.getPosition().x + face.getWidth()),
                Math.round(face.getPosition().y + face.getHeight())
        );

        Rect mappedBounds;

        switch (imageRotationCompensation) {
            case 0:
                mappedBounds = new Rect(bounds);
                break;
            case 1:
                mappedBounds = new Rect(bounds.top, rawImage.getHeight() - bounds.right, bounds.bottom, rawImage.getHeight() - bounds.left);
                break;
            case 2:
                mappedBounds = new Rect(rawImage.getWidth() - bounds.right, rawImage.getHeight() - bounds.bottom, rawImage.getWidth() - bounds.left, rawImage.getHeight() - bounds.top);
                break;
            case 3:
                mappedBounds = new Rect(rawImage.getWidth() - bounds.bottom, bounds.left, rawImage.getWidth() - bounds.top, bounds.right);
                break;
            default:
                throw new IllegalArgumentException("Unsupported image rotation angle: " + imageRotationCompensation);
        }

        float angle = AngleUtils.normaliseAngle(face.getEulerZ() + offset);
        return new FaceDetectionFace(mappedBounds, angle);
    }

    private void close() {
        if (!isRunning)
            throw new IllegalStateException("Already closed");

        listener = null;
        result = null;

        handler.removeCallbacks(sendResult);
        handler = null;

        stopwatch.stop();
        stopwatch = null;

        isRunning = false;
    }

    @Override
    public void cancel() {
        if (!isRunning)
            throw new IllegalStateException("Already closed");

        FaceDetectionListener listenerBackup = this.listener;
        close();
        listenerBackup.onCancelled();
    }

    private final Runnable sendResult = new Runnable() {
        @Override
        public void run() {
            if (isRunning) {
                Log.v(TAG, "Detection took " + stopwatch.getElapsedMillis() + "ms");

                if (result.prominentFace != null)
                    Log.v(TAG, "Got face: " + result.prominentFace.angle);

                FaceDetectionListener listenerBackup = listener;
                FaceDetectionResult resultBackup = result;
                close();
                listenerBackup.onDetectionResult(resultBackup);
            }
        }
    };

}
