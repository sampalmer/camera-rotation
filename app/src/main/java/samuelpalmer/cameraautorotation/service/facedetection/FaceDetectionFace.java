package samuelpalmer.cameraautorotation.service.facedetection;

import android.graphics.Rect;

public class FaceDetectionFace {

    /**
     * The approximate area that contains the face. The coordinates are relative to the original
     * image without taking into account any rotations applied to the image.
     */
    public final Rect bounds;

    /**
     * The angle of the user's face relative to the device's natural orientation.
     * This angle is clockwise, assuming you are looking at the back of the user's head.
     * The is in the range [0, 360):
     * 0 = user's head is perfectly aligned with device;
     * 90 = user's head is on right side compared to device;
     * 180 = user's head is upside-down compared to device;
     * 270 = user's head is on left side compared to device;
     */
    public final float angle;

    public FaceDetectionFace(Rect bounds, float angle) {
        if (Float.isNaN(angle) || Float.isInfinite(angle))
            throw new IllegalArgumentException("Angle must be finite, but instead found " + angle);

        if (angle < 0 || angle >= 360)
            throw new IllegalArgumentException("Angle must be normalised to [0, 360), but instead found " + angle);

        this.bounds = bounds;
        this.angle = angle;
    }

}
