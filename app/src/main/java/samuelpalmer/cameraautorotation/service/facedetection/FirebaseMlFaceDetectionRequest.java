package samuelpalmer.cameraautorotation.service.facedetection;

import android.graphics.Rect;
import android.media.Image;
import android.util.Log;
import android.util.SparseArray;

import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import samuelpalmer.cameraautorotation.utils.AngleUtils;
import samuelpalmer.cameraautorotation.utils.Stopwatch;

class FirebaseMlFaceDetectionRequest implements FaceDetectionRequest {

    private static final String TAG = FirebaseMlFaceDetectionRequest.class.getSimpleName();

    private SparseArray<FirebaseVisionFaceDetector> detectorsByOffset;
    private float predictedFaceAngle;
    private Image rawImage;
    private int rotationCompensationClockwiseDegrees;
    private FaceDetectionListener listener;
    private Stopwatch stopwatch;
    private boolean isDetecting;
    private Integer[] prioritisedOffsets;

    public FirebaseMlFaceDetectionRequest(SparseArray<FirebaseVisionFaceDetector> detectorsByOffset, float predictedFaceAngle, Image rawImage, int rotationCompensationClockwiseDegrees, FaceDetectionListener listener) {
        this.detectorsByOffset = detectorsByOffset;
        this.predictedFaceAngle = predictedFaceAngle;
        this.rawImage = rawImage;
        this.rotationCompensationClockwiseDegrees = rotationCompensationClockwiseDegrees;
        this.listener = listener;
        stopwatch = Stopwatch.startNew();
        isDetecting = true;

        prioritisedOffsets = new Integer[] { 0, 90, 270, 180 };

        if (!Float.isNaN(predictedFaceAngle))
            // We'll try using the offsets that are closest to the last known face angle to improve performance
            Arrays.sort(prioritisedOffsets, (offsetA, offsetB) -> {
                float offsetADifferenceFromLastFaceAngle = AngleUtils.normaliseAngleDifference(offsetA - predictedFaceAngle);
                float offsetBDifferenceFromLastFaceAngle = AngleUtils.normaliseAngleDifference(offsetB - predictedFaceAngle);
                return Float.compare(offsetADifferenceFromLastFaceAngle, offsetBDifferenceFromLastFaceAngle);
            });

        detect(0);
    }

    @Override
    public void cancel() {
        if (!isDetecting)
            throw new IllegalStateException("Not running");

        FaceDetectionListener listenerBackup = this.listener;
        close();
        listenerBackup.onCancelled();
    }

    private void close() {
        if (!isDetecting)
            throw new IllegalStateException("Not running");

        prioritisedOffsets = null;

        isDetecting = false;
        stopwatch.stop();
        stopwatch = null;

        listener = null;
        rotationCompensationClockwiseDegrees = 0;
        rawImage = null;
        predictedFaceAngle = 0;
        detectorsByOffset = null;
    }

    private void detect(int i) {
        if (!isDetecting)
            throw new IllegalStateException("Not running");

        int offset = prioritisedOffsets[i];

        Log.v(TAG, "Starting detection at " + offset);

        int imageRotationCompensation = FirebaseVisionImageMetadata.ROTATION_0 + ((rotationCompensationClockwiseDegrees + offset) % 360) / 90;

        // This seems to create a copy of the original image, so there's no need to hang onto the original
        /* TODO: To improve performance and accuracy, combine the camera's built-in face detection with this:
            If the built-in detector detects eye positions, use those to deduce the face angle
       */
        FirebaseVisionImage firebaseImage = FirebaseVisionImage.fromMediaImage(rawImage, imageRotationCompensation);

        // Now that Firebase Vision has read in the image, we need to reset its buffer position back to the start for the next operation
        Image.Plane[] imagePlanes = rawImage.getPlanes();
        if (imagePlanes != null)
            for (Image.Plane plane : imagePlanes) {
                ByteBuffer buffer = plane.getBuffer();
                if (buffer != null)
                    buffer.clear();
            }

        FirebaseVisionFaceDetector detector = detectorsByOffset.get(offset);

        FaceDetectionListener listenerBackup = this.listener;

        detector.detectInImage(firebaseImage)
                .addOnSuccessListener(detectedFaces -> {
                    if (isDetecting) {
                        Log.v(TAG, "Face detection successful for offset " + offset + ". Found " + detectedFaces.size() + " face(s).");

                        FaceDetectionResult newResult = buildResult(detectedFaces, offset, imageRotationCompensation);
                        int nextI = i + 1;

                        if (newResult.foundFaces() || nextI >= prioritisedOffsets.length) {
                            Log.v(TAG, "Detection took " + stopwatch.getElapsedMillis() + "ms");

                            if (!newResult.foundFaces())
                                Log.w(TAG, "No faces detected");

                            close();
                            listenerBackup.onDetectionResult(newResult);
                        } else {
                            detect(nextI);
                        }
                    }
                })
                .addOnFailureListener(e -> {
                    if (isDetecting)
                        // This probably means there's a bug in the app code, so might as well give up altogether
                        throw new RuntimeException("Face detection failed", e);
                })
                .addOnCompleteListener(task -> {
                    if (isDetecting)
                        Log.v(TAG, "Face detection complete");
                })
                .addOnCanceledListener(() -> {
                    if (isDetecting) {
                        Log.w(TAG, "Face detection cancelled");
                        close();
                        listenerBackup.onDetectionResult(new FaceDetectionResult());
                    }
                });
    }

    private FaceDetectionResult buildResult(List<FirebaseVisionFace> detectedFaces, int offsetDegrees, int imageRotationCompensation) {
        // Unfortunately, Firebase has a lot of false positives, so we need to eliminate them to keep the results accurate
        List<FirebaseVisionFace> validFaces = new ArrayList<>();
        for (FirebaseVisionFace face : detectedFaces)
            if (isValidFace(face))
                validFaces.add(face);

        // There might be multiple faces detected, so we'll need to figure out which one is most suitable
        // TODO: Also prioritise faces closer to the centre of the camera. Eg the main user might be sitting in front of the phone while someone else leans over, resulting in the other person's face being bigger and hence chosen
        // TODO: Also, if there are several faces, prioritise the ones that are all oriented the same way. OR maybe just take the average?
        // TODO: Also prioritise people who are looking at the device (Euler Y angle)
        FirebaseVisionFace bestFace = null;
        for (FirebaseVisionFace face : validFaces) {
            if (isValidFace(face)) {
                if (bestFace == null)
                    bestFace = face;
                else if (!Float.isNaN(predictedFaceAngle) && differenceFromLastFace(offsetDegrees, face) < differenceFromLastFace(offsetDegrees, bestFace))
                    // We're already tracking a particular face, so we'll prefer that one to avoid flapping
                    bestFace = face;
                else if (getArea(face) > getArea(bestFace))
                    // Use the biggest face since it's probably closest to the screen
                    // Also, the library sometimes thinks background objects or small details are faces, so this helps eliminate them
                    bestFace = face;
            }
        }

        // Build up result from Firebase face data
        FaceDetectionFace[] resultFaces = new FaceDetectionFace[validFaces.size()];
        FaceDetectionFace resultProminentFace = null;
        for (int i = 0; i < validFaces.size(); ++i) {
            FirebaseVisionFace face = validFaces.get(i);
            FaceDetectionFace resultFace = faceToResultFace(face, offsetDegrees, imageRotationCompensation);
            resultFaces[i] = resultFace;

            if (bestFace != null && face == bestFace)
                resultProminentFace = resultFace;
        }

        if (resultProminentFace != null)
            Log.i(TAG, "Got face: " + Math.round(resultProminentFace.angle) + ", face: " + bestFace.toString());

        return new FaceDetectionResult(resultProminentFace, resultFaces);
    }

    private boolean isValidFace(FirebaseVisionFace face) {
        // Note that eye landmark positions are more accurate than eye contours
        // TODO: Consider using the angle between the eyes instead of the face angle. See if it's more accurate?
        // TODO: Build a confidence score to deteremine which faces to include. See https://stackoverflow.com/a/54226871/238753

        /* _Indicators_ of an invalid face:
         * - Small bounding box in proportion to total frame size
         * - Lower eye closed probability
         */

        // People who aren't looking at the phone can't see the screen so shouldn't be included.
        // This might help eliminate false positives in the underlying library.
        if (Math.abs(face.getHeadEulerAngleY()) > 50)
            return false;

        // Faces that are partially out of frame at the top or bottom get inaccurate angle values, so we're filtering them out
        float boundingBoxRatio = (float) face.getBoundingBox().width() / (float) face.getBoundingBox().height();
        if (boundingBoxRatio >= 1.7)
            return false;

        // Sometimes the library returns a face with a valid bounding box but default values for all other fields.
        // So something must have gone wrong. These faces seem to be incorrect more often than not.
        // And the face angle isn't available anyway, so they can't be used.
        if (face.getHeadEulerAngleZ() == 0 || face.getHeadEulerAngleY() == 0)
            return false;

        return true;
    }

    private static int getArea(FirebaseVisionFace face) {
        Rect boundingBox = face.getBoundingBox();
        return Math.abs(boundingBox.width()) * Math.abs(boundingBox.height());
    }

    private float differenceFromLastFace(int offsetDegrees, FirebaseVisionFace face) {
        float faceAngle = face.getHeadEulerAngleZ() + offsetDegrees;
        return AngleUtils.normaliseAngleDifference(faceAngle - predictedFaceAngle);
    }

    @SuppressWarnings("SuspiciousNameCombination")
    private FaceDetectionFace faceToResultFace(FirebaseVisionFace face, int offsetDegrees, int imageRotationCompensation) {
        Rect bounds = face.getBoundingBox();
        Rect mappedBounds;

        switch (imageRotationCompensation) {
            case 0:
                mappedBounds = new Rect(bounds);
                break;
            case 1:
                mappedBounds = new Rect(bounds.top, rawImage.getHeight() - bounds.right, bounds.bottom, rawImage.getHeight() - bounds.left);
                break;
            case 2:
                mappedBounds = new Rect(rawImage.getWidth() - bounds.right, rawImage.getHeight() - bounds.bottom, rawImage.getWidth() - bounds.left, rawImage.getHeight() - bounds.top);
                break;
            case 3:
                mappedBounds = new Rect(rawImage.getWidth() - bounds.bottom, bounds.left, rawImage.getWidth() - bounds.top, bounds.right);
                break;
            default:
                throw new IllegalArgumentException("Unsupported image rotation angle: " + imageRotationCompensation);
        }

        float angle = AngleUtils.normaliseAngle(face.getHeadEulerAngleZ() + offsetDegrees);
        return new FaceDetectionFace(mappedBounds, angle);
    }

}
