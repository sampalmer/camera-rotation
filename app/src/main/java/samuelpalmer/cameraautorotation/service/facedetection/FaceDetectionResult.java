package samuelpalmer.cameraautorotation.service.facedetection;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class FaceDetectionResult {

    /**
     * The main face in the image. Guaranteed to be present in {@link #allFaces}.
     */
    @Nullable
    public final FaceDetectionFace prominentFace;

    /**
     * The complete set of detected faces, including {@link #prominentFace}.
     */
    @NonNull
    public final FaceDetectionFace[] allFaces;

    public FaceDetectionResult() {
        this(null, new FaceDetectionFace[0]);
    }

    FaceDetectionResult(@Nullable FaceDetectionFace prominentFace, @NonNull FaceDetectionFace[] allFaces) {
        if (allFaces.length > 0) {
            if (prominentFace == null)
                throw new IllegalArgumentException("Prominent face cannot be null when faces were detected");

            boolean foundProminentFace = false;
            for (FaceDetectionFace face : allFaces)
                if (face == prominentFace) {
                    foundProminentFace = true;
                    break;
                }

            if (!foundProminentFace)
                throw new IllegalArgumentException("Prominent face must be included in all faces");
        }

        this.prominentFace = prominentFace;
        this.allFaces = allFaces;
    }

    public boolean foundFaces() {
        return allFaces.length > 0;
    }

}
