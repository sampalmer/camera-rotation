package samuelpalmer.cameraautorotation.service.facedetection;

import android.media.Image;
import android.util.Log;
import android.util.SparseArray;

import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions;

import java.io.IOException;

public class FirebaseMlFaceDetector implements FaceDetector {

    private static final String TAG = FirebaseMlFaceDetector.class.getSimpleName();

    private SparseArray<FirebaseVisionFaceDetector> detectorsByOffset;
    private float predictedUserAngle = Float.NaN; // TODO: Infer the initial value from the screen rotation
    private FirebaseMlFaceDetectionRequest request;

    public FirebaseMlFaceDetector() {
        detectorsByOffset = new SparseArray<>();
        for (int offset = 0; offset < 360; offset += 90) {
            FirebaseVisionFaceDetectorOptions detectorOptions = new FirebaseVisionFaceDetectorOptions.Builder()
                    .setClassificationMode(FirebaseVisionFaceDetectorOptions.NO_CLASSIFICATIONS)
                    .setContourMode(FirebaseVisionFaceDetectorOptions.NO_CONTOURS)
                    .setLandmarkMode(FirebaseVisionFaceDetectorOptions.NO_LANDMARKS)

                    // Increasing minimum face size incrases performance and eliminates some tiny invalid faces that the library picks up.
                    // Unfortunately, this causes it to occasionally not detect really obvious faces, so we need to keep it small.
                    // Slightly varying the size to prevent the library from re-using the same detector instance, which will mess up face tracking across different image orientations
                    .setMinFaceSize(1f - (float)offset / 10000f) // TODO: Factor in image orientation and camera's field-of-view to make this consistent across devices
                    .setPerformanceMode(FirebaseVisionFaceDetectorOptions.FAST)// TODO: Investigate whether this affects detection accuracy in poor lighting
                    .enableTracking() // This significantly improves performance. However, it also causes the detector to get mixed up since we're feeding in multiple image rotations. We're working around this by using multiple detectors, which negates most performance improvements but is still better than before.
                    .build();

            FirebaseVisionFaceDetector detector = FirebaseVision.getInstance()
                    .getVisionFaceDetector(detectorOptions);
            detectorsByOffset.put(offset, detector);
        }

        if (detectorsByOffset.get(0) == detectorsByOffset.get(90))
            throw new RuntimeException("Got multiple instances of the same detector. The results will be inaccurate since face tracking will occur across different rotations of each image.");
    }

    public void close() {
        if (detectorsByOffset == null)
            throw new IllegalStateException();

        if (request != null) {
            request.cancel();
            request = null;
        }

        predictedUserAngle = Float.NaN;

        for (int i = 0; i < detectorsByOffset.size(); ++i) {
            FirebaseVisionFaceDetector detector = detectorsByOffset.valueAt(i);

            try {
                detector.close();
            } catch (IOException e) {
                Log.w(TAG, "Failed to close detector", e);
            }
        }

        detectorsByOffset = null;
    }

    /**
     * @param rawImage Will be held until detection is complete.
     * @param rotationCompensationClockwiseDegrees The number of degrees by which the image should be rotated clockwise to align it with the device's natural orientation.
     */
    public FaceDetectionRequest detectInImage(Image rawImage, int rotationCompensationClockwiseDegrees, FaceDetectionListener listener) {
        if (detectorsByOffset == null)
            throw new IllegalStateException();

        if (request != null)
            throw new IllegalStateException("Already performing detection");

        Log.v(TAG, "Starting detection");

        request = new FirebaseMlFaceDetectionRequest(detectorsByOffset, predictedUserAngle, rawImage, rotationCompensationClockwiseDegrees, new FaceDetectionListener() {
            @Override
            public void onDetectionResult(FaceDetectionResult result) {
                if (detectorsByOffset != null && request != null) {
                    request = null;
                    listener.onDetectionResult(result);
                }
            }

            @Override
            public void onCancelled() {
                if (detectorsByOffset != null && request != null) {
                    request = null;
                    listener.onCancelled();
                }
            }
        });

        return request;
    }

    @Override
    public void setPredictedUserAngle(float predictedUserAngle) {
        this.predictedUserAngle = predictedUserAngle;
    }

}
