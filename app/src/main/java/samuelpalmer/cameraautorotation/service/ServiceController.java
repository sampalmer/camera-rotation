package samuelpalmer.cameraautorotation.service;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.content.ContextCompat;

public abstract class ServiceController {

    private static final String TAG = ServiceController.class.getSimpleName();

    public static void startService(Context context) {
        // TODO: The OS crashes when the start and stop buttons are mashed: android.app.RemoteServiceException: Context.startForegroundService() did not then call Service.startForeground(). This looks like an OS bug, since the service consistently calls startForeground() as needed.
        Log.i(TAG, "Starting service");

        Intent intent = makeIntent(context);
        ContextCompat.startForegroundService(context, intent);
    }

    public static void stopService(Context context) {
        Log.i(TAG, "Stopping service");

        Intent intent = makeIntent(context);
        context.stopService(intent);
    }

    private static Intent makeIntent(Context context) {
        return new Intent(context, MainService.class);
    }

}
