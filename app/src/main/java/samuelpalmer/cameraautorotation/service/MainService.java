package samuelpalmer.cameraautorotation.service;

import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleService;

public class MainService extends LifecycleService {

    private static final String TAG = MainService.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i(TAG, "Service created");

        // The notification needs to be added before anything else so the OS doesn't timeout the service for taking too long to startForeground()
        getLifecycle().addObserver(new ServiceNotification(this));
        getLifecycle().addObserver(new CameraScreenStateSynchroniser(this));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "Service destroyed");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Log.i(TAG, "Service started");
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

}
