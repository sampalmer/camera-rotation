package samuelpalmer.cameraautorotation.service;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;

import samuelpalmer.cameraautorotation.utils.DeviceInteractivityMonitor;

/**
 * Controls whether to run the camera depending on whether the screen is on.
 * We don't want the camera to run when the screen is off since it could drain the battery.
 */
class CameraScreenStateSynchroniser implements DefaultLifecycleObserver {

    private final Context context;

    private MainAlgorithm mainAlgorithm;
    private DeviceInteractivityMonitor interactivityMonitor;

    public CameraScreenStateSynchroniser(Context context) {
        this.context = context;
    }

    @Override
    public void onStart(@NonNull LifecycleOwner owner) {
        mainAlgorithm = new MainAlgorithm(context);
        interactivityMonitor = new DeviceInteractivityMonitor(context, () -> {
            if (owner.getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.CREATED))
                updateCameraState();
        });
        interactivityMonitor.start();

        updateCameraState();
    }

    @Override
    public void onStop(@NonNull LifecycleOwner owner) {
        interactivityMonitor.stop();
        interactivityMonitor = null;

        mainAlgorithm.close();
        mainAlgorithm = null;
    }

    private void updateCameraState() {
        boolean shouldUseCamera = interactivityMonitor.isInteractive();

        if (mainAlgorithm.isStarted() != shouldUseCamera)
            if (mainAlgorithm.isStarted())
                mainAlgorithm.stop();
            else
                mainAlgorithm.start();
    }

}
