package samuelpalmer.cameraautorotation.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import samuelpalmer.cameraautorotation.ApplicationSettings;

/**
 * When the app is upgraded or the device is restarted, then the OS doesn't automatically restart
 * the service. This class detects these events and restarts the service if needed.
 *
 * NOTE THAT THIS WON'T ALWAYS BE CALLED WHEN USING ANDROID STUDIO'S INSTANT RUN FEATURE
 */
public class StartServiceReceiver extends BroadcastReceiver {

    private static final String TAG = StartServiceReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "Received intent: " + intent.getAction());

        if (!Intent.ACTION_MY_PACKAGE_REPLACED.equals(intent.getAction()) && !Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Log.w(TAG, "Unknown action: " + intent.getAction());
            return;
        }

        SharedPreferences prefs = ApplicationSettings.getSharedPreferences(context);
        boolean isSwitchedOn = prefs.getBoolean(ApplicationSettings.KEY_IS_ON, false);
        if (isSwitchedOn)
            ServiceController.startService(context);
    }

}
