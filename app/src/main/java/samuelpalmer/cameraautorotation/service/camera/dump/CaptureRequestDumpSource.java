package samuelpalmer.cameraautorotation.service.camera.dump;

import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.MeteringRectangle;
import android.os.Build;

class CaptureRequestDumpSource extends CameraDumpSource {

    private final CaptureRequest request;

    public CaptureRequestDumpSource(CameraCharacteristics characteristics, CaptureRequest request) {
        super(characteristics);
        this.request = request;

        append(wrap(CaptureRequest.CONTROL_MODE), controlMode -> getValueName(controlMode, "CONTROL_MODE_"));
        append(wrap(CaptureRequest.CONTROL_SCENE_MODE), sceneMode -> getValueName(sceneMode, "CONTROL_SCENE_MODE_"));

        append(wrap(CaptureRequest.CONTROL_AF_MODE), afMode -> getValueName(afMode, "CONTROL_AF_MODE_"));
        appendArray(wrap(CaptureRequest.CONTROL_AF_REGIONS), MeteringRectangle::toString);
        append(wrap(CaptureRequest.CONTROL_AF_TRIGGER), afTrigger -> getValueName(afTrigger, "CONTROL_AF_TRIGGER_"));

        append(wrap(CaptureRequest.CONTROL_AE_MODE), aeMode -> getValueName(aeMode, "CONTROL_AE_MODE_"));
        append(wrap(CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION), this::formatExposureCompensation);
        append(wrap(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER), precaptureTrigger -> getValueName(precaptureTrigger, "CONTROL_AE_PRECAPTURE_TRIGGER_"));
        append(wrap(CaptureRequest.CONTROL_AE_LOCK));
        appendArray(wrap(CaptureRequest.CONTROL_AE_REGIONS), MeteringRectangle::toString);
        append(wrap(CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE));

        append(wrap(CaptureRequest.CONTROL_AWB_MODE), awbMode -> getValueName(awbMode, "CONTROL_AWB_MODE_"));
        append(wrap(CaptureRequest.CONTROL_AWB_LOCK));
        appendArray(wrap(CaptureRequest.CONTROL_AWB_REGIONS), MeteringRectangle::toString);

        append(wrap(CaptureRequest.CONTROL_CAPTURE_INTENT), captureIntent -> getValueName(captureIntent, "CONTROL_CAPTURE_INTENT_"));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            append(wrap(CaptureRequest.CONTROL_POST_RAW_SENSITIVITY_BOOST));

        append(wrap(CaptureRequest.FLASH_MODE), flashMode -> getValueName(flashMode, "FLASH_MODE_"));
        append(wrap(CaptureRequest.LENS_APERTURE));
        append(wrap(CaptureRequest.SENSOR_EXPOSURE_TIME), exposureTimeNanos -> exposureTimeNanos + "ns");
        append(wrap(CaptureRequest.SENSOR_FRAME_DURATION), frameDuration -> frameDuration + "ns");
        append(wrap(CaptureRequest.SENSOR_SENSITIVITY));
        append(wrap(CaptureRequest.STATISTICS_FACE_DETECT_MODE), faceDetectMode -> getValueName(faceDetectMode, "STATISTICS_FACE_DETECT_MODE_"));
        append(wrap(CaptureRequest.LENS_OPTICAL_STABILIZATION_MODE), opticalStabilizationMode -> getValueName(opticalStabilizationMode, "LENS_OPTICAL_STABILIZATION_MODE_"));
        append(wrap(CaptureRequest.LENS_FOCAL_LENGTH), focalLengthMm -> focalLengthMm + "mm");
        append(wrap(CaptureRequest.LENS_APERTURE), aperture -> "f/" + aperture);
    }

    private <TValue> KeyAdaptor<TValue> wrap(CaptureRequest.Key<TValue> key) {
        return new CaptureRequestKeyAdaptor<>(request, key);
    }

}
