package samuelpalmer.cameraautorotation.service.camera.dump;

import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.params.MeteringRectangle;
import android.os.Build;

class CaptureResultDumpSource extends CameraDumpSource {

    private final CaptureResult result;

    public CaptureResultDumpSource(CameraCharacteristics cameraCharacteristics, CaptureResult result) {
        super(cameraCharacteristics);
        this.result = result;

        append(wrap(CaptureResult.CONTROL_MODE), controlMode -> getValueName(controlMode, "CONTROL_MODE_"));
        append(wrap(CaptureResult.CONTROL_SCENE_MODE), sceneMode -> getValueName(sceneMode, "CONTROL_SCENE_MODE_"));

        append(wrap(CaptureResult.CONTROL_AF_MODE), afMode -> getValueName(afMode, "CONTROL_AF_MODE_"));
        appendArray(wrap(CaptureResult.CONTROL_AF_REGIONS), MeteringRectangle::toString);
        append(wrap(CaptureResult.CONTROL_AF_TRIGGER), afTrigger -> getValueName(afTrigger, "CONTROL_AF_TRIGGER_"));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
            append(wrap(CaptureResult.CONTROL_AF_SCENE_CHANGE), afSceneChange -> getValueName(afSceneChange, "CONTROL_AF_SCENE_CHANGE_"));
        append(wrap(CaptureResult.CONTROL_AF_STATE), afState -> getValueName(afState, "CONTROL_AF_STATE_"));

        append(wrap(CaptureResult.CONTROL_AE_MODE), aeMode -> getValueName(aeMode, "CONTROL_AE_MODE_"));
        append(wrap(CaptureResult.CONTROL_AE_EXPOSURE_COMPENSATION), this::formatExposureCompensation);
        append(wrap(CaptureResult.CONTROL_AE_PRECAPTURE_TRIGGER), precaptureTrigger -> getValueName(precaptureTrigger, "CONTROL_AE_PRECAPTURE_TRIGGER_"));
        append(wrap(CaptureResult.CONTROL_AE_LOCK));
        appendArray(wrap(CaptureResult.CONTROL_AE_REGIONS), MeteringRectangle::toString);
        append(wrap(CaptureResult.CONTROL_AE_TARGET_FPS_RANGE));
        append(wrap(CaptureResult.CONTROL_AE_STATE), aeState -> getValueName(aeState, "CONTROL_AE_STATE_"));

        append(wrap(CaptureResult.CONTROL_AWB_MODE), awbMode -> getValueName(awbMode, "CONTROL_AWB_MODE_"));
        append(wrap(CaptureResult.CONTROL_AWB_LOCK));
        appendArray(wrap(CaptureResult.CONTROL_AWB_REGIONS), MeteringRectangle::toString);
        append(wrap(CaptureResult.CONTROL_AWB_STATE), awbState -> getValueName(awbState, "CONTROL_AWB_STATE_"));

        append(wrap(CaptureResult.CONTROL_CAPTURE_INTENT), captureIntent -> getValueName(captureIntent, "CONTROL_CAPTURE_INTENT_"));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            append(wrap(CaptureResult.CONTROL_POST_RAW_SENSITIVITY_BOOST));

        append(wrap(CaptureResult.FLASH_MODE), flashMode -> getValueName(flashMode, "FLASH_MODE_"));
        append(wrap(CaptureResult.FLASH_STATE), flashState -> getValueName(flashState, "FLASH_STATE_"));

        append(wrap(CaptureResult.LENS_APERTURE));
        append(wrap(CaptureResult.SENSOR_EXPOSURE_TIME), exposureTimeNanos -> exposureTimeNanos + "ns");
        append(wrap(CaptureResult.SENSOR_FRAME_DURATION), frameDuration -> frameDuration + "ns");
        append(wrap(CaptureResult.SENSOR_SENSITIVITY));
        append(wrap(CaptureResult.STATISTICS_FACE_DETECT_MODE), faceDetectMode -> getValueName(faceDetectMode, "STATISTICS_FACE_DETECT_MODE_"));
        append(wrap(CaptureResult.LENS_OPTICAL_STABILIZATION_MODE), opticalStabilizationMode -> getValueName(opticalStabilizationMode, "LENS_OPTICAL_STABILIZATION_MODE_"));
        append(wrap(CaptureResult.LENS_FOCAL_LENGTH), focalLengthMm -> focalLengthMm + "mm");
        append(wrap(CaptureResult.LENS_APERTURE), aperture -> "f/" + aperture);
    }
    
    private <TValue> KeyAdaptor<TValue> wrap(CaptureResult.Key<TValue> key) {
        return new CaptureResultKeyAdaptor<>(result, key);
    }
    
}
