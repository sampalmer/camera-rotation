package samuelpalmer.cameraautorotation.service.camera.dump;

import android.hardware.camera2.CameraCharacteristics;

class CameraCharacteristicsKeyAdaptor<TValue> implements KeyAdaptor<TValue> {

    private final CameraCharacteristics cameraCharacteristics;
    private final CameraCharacteristics.Key<TValue> key;

    public CameraCharacteristicsKeyAdaptor(CameraCharacteristics cameraCharacteristics, CameraCharacteristics.Key<TValue> key) {
        this.cameraCharacteristics = cameraCharacteristics;
        this.key = key;
    }

    @Override
    public String getName() {
        return key.getName();
    }

    @Override
    public TValue getValue() {
        return cameraCharacteristics.get(key);
    }

}
