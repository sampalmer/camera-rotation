package samuelpalmer.cameraautorotation.service.camera;

import android.graphics.ImageFormat;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.util.Log;
import android.util.Range;
import android.view.Surface;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import samuelpalmer.cameraautorotation.service.camera.dump.CameraDump;
import samuelpalmer.cameraautorotation.service.facedetection.FaceDetectionListener;
import samuelpalmer.cameraautorotation.service.facedetection.FaceDetectionRequest;
import samuelpalmer.cameraautorotation.service.facedetection.FaceDetectionResult;
import samuelpalmer.cameraautorotation.service.facedetection.FaceDetector;
import samuelpalmer.cameraautorotation.utils.BitmapUtils;
import samuelpalmer.cameraautorotation.utils.FieldConstants;
import samuelpalmer.cameraautorotation.utils.TimestampComparer;

public class CameraMonitorRequest {

    private static final int TIMEOUT_MILLIS = 2500;
    private static final String TAG = CameraMonitorRequest.class.getSimpleName();

    private enum State {
        NOT_RUNNING,
        TAKING_PICTURES,
        DETECTING_FACE
    }

    private final ImageReader imageReader;
    private final FaceDetector detector;
    private final CameraCaptureSession captureSession;
    private final CameraCharacteristics cameraCharacteristics;
    private final Listener listener;
    private final Handler handler;

    private State state;
    private Image lastImage;
    private CaptureResult lastResult;
    private FaceDetectionRequest detectionRequest;
    private long firstImageTimestampNanos;
    private boolean timedOut;
    private boolean hasRetried;

    public interface Listener {
        void onDetectionComplete(FaceDetectionResult result, Image image);
        void onCameraLost(@Nullable CameraAccessException e);
    }

    public CameraMonitorRequest(ImageReader imageReader, FaceDetector detector, CameraDevice camera, CameraCaptureSession captureSession, CameraCharacteristics cameraCharacteristics, Range<Integer> targetFpsRange, Listener listener) throws CameraAccessException {
        this.imageReader = imageReader;
        this.detector = detector;
        this.captureSession = captureSession;
        this.cameraCharacteristics = cameraCharacteristics;
        this.listener = listener;
        this.handler = new Handler();

        // We need to dequeue and close any leftover images so our new picture gets sent to the ImageReader
        Image leftoverImage = imageReader.acquireLatestImage();
        if (leftoverImage != null) {
            Log.w(TAG, "Found leftover image blocking the ImageReader. Removing.");
            leftoverImage.close();
        }

        imageReader.setOnImageAvailableListener(imageAvailableCallback, null);

        CaptureRequest.Builder requestBuilder = camera.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);

        // The first image must be our YUV image for face detection to ensure compatibility with HARDWARE_LEVEL_LEGACY
        requestBuilder.addTarget(imageReader.getSurface());

        // Enable auto-exposure etc
        requestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);

        // We *must* use auto exposure to make sure we can actually see the user's face properly

        // The documentation for CameraCharacteristics.CONTROL_AE_AVAILABLE_MODES says all camera devices support "on"
        requestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CameraMetadata.CONTROL_AE_MODE_ON);
        requestBuilder.set(CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE, targetFpsRange);
        requestBuilder.set(CaptureRequest.CONTROL_AE_LOCK, false);

        // The documentation for CameraCharacteristics.CONTROL_AWB_AVAILABLE_MODES says all camera devices support "on"
        requestBuilder.set(CaptureRequest.CONTROL_AWB_MODE, CameraMetadata.CONTROL_AWB_MODE_AUTO);
        requestBuilder.set(CaptureRequest.CONTROL_AWB_LOCK, false);

        // Don't want to blind the user!
        requestBuilder.set(CaptureRequest.FLASH_MODE, CameraMetadata.FLASH_MODE_OFF);

        // Using "preview" intent to encourage use of auto-exposure and focus more on performance than image quality
        requestBuilder.set(CaptureRequest.CONTROL_CAPTURE_INTENT, CameraMetadata.CONTROL_CAPTURE_INTENT_PREVIEW);

        // TODO: Consider setting up and using auto-focus
        // TODO: Consider turning on HDR to help make faces visible even if the background is bright (eg sun or light globe behind user): CaptureRequest.CONTROL_SCENE_MODE_HDR

        requestBuilder.set(CaptureRequest.LENS_FOCAL_LENGTH, selectBestFocalLength());

        CaptureRequest request = requestBuilder.build();
        Log.v(TAG, "Capture request:\n" + CameraDump.dump(cameraCharacteristics, request));

        // In order for auto-exposure to run quickly on non-legacy devices, we need to run at full framerate
        state = State.TAKING_PICTURES;
        firstImageTimestampNanos = 0;

        timedOut = false;
        handler.postDelayed(onTimeout, TIMEOUT_MILLIS);

        hasRetried = false;

        captureSession.setRepeatingRequest(request, captureCallback, null);
    }

    /**
     * Most devices are fixed focus, but the Pixel 3 XL actually has two available focal lengths.
     */
    private float selectBestFocalLength() {
        float bestFocalLength = Float.NaN;

        float[] availableFocalLengths = cameraCharacteristics.get(CameraCharacteristics.LENS_INFO_AVAILABLE_FOCAL_LENGTHS);
        assert availableFocalLengths != null; // The docs say this is always available
        for (float focalLength : availableFocalLengths) {
            // We want the smallest focal length since this results in the largest field of view,
            // which means the user's face is less likely to be out-of-frame
            if (Float.isNaN(bestFocalLength) || focalLength < bestFocalLength)
                // The docs say the focal lengths are already in ascending order, but that isn't the case with the Pixel 3 XL
                bestFocalLength = focalLength;
        }

        return bestFocalLength;
    }

    public void close() {
        if (state == State.NOT_RUNNING)
            throw new IllegalStateException("Not running");

        state = State.NOT_RUNNING;

        try {
            captureSession.stopRepeating();
        } catch (IllegalStateException | CameraAccessException ignored) {
            // We're in the middle of closing, so it's ok if the camera has already died and the session is already closed
        }

        hasRetried = false;

        handler.removeCallbacks(onTimeout);
        timedOut = false;

        firstImageTimestampNanos = 0;
        lastResult = null;

        if (lastImage != null) {
            lastImage.close();
            lastImage = null;
        }

        // This must happen after closing the latest image. Otherwise we may get an error.
        Image leftoverImage = imageReader.acquireLatestImage();
        if (leftoverImage != null) {
            Log.v(TAG, "Found leftover image: " + leftoverImage.getTimestamp() + ". Dropping.");
            leftoverImage.close();
        }

        if (detectionRequest != null) {
            detectionRequest.cancel();
            detectionRequest = null;
        }

        // Keeping the image listener attached so it can drop any leftover images.
        // Otherwise the ImageReader will be full and won't receive any subsequent pictures from the camera.
    }

    private final CameraCaptureSession.CaptureCallback captureCallback = new CameraCaptureSession.CaptureCallback() {

        @Override
        public void onCaptureStarted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, long timestamp, long frameNumber) {
            super.onCaptureStarted(session, request, timestamp, frameNumber);
            Log.v(TAG, "Capture started");
        }

        @Override
        public void onCaptureProgressed(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull CaptureResult partialResult) {
            super.onCaptureProgressed(session, request, partialResult);
            Log.v(TAG, "Capture progressed");
        }

        @Override
        public void onCaptureSequenceAborted(@NonNull CameraCaptureSession session, int sequenceId) {
            super.onCaptureSequenceAborted(session, sequenceId);
            Log.i(TAG, "Capture sequence aborted");
        }

        @Override
        public void onCaptureSequenceCompleted(@NonNull CameraCaptureSession session, int sequenceId, long frameNumber) {
            super.onCaptureSequenceCompleted(session, sequenceId, frameNumber);
            Log.v(TAG, "Capture sequence completed");
        }

        @Override
        public void onCaptureBufferLost(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull Surface target, long frameNumber) {
            super.onCaptureBufferLost(session, request, target, frameNumber);
            Log.v(TAG, "Capture buffer lost");
        }

        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
            super.onCaptureCompleted(session, request, result);

            //noinspection ConstantConditions Since the docs say this is always available
            long newResultTimestamp = result.get(CaptureResult.SENSOR_TIMESTAMP);

            Log.v(TAG, "Capture completed: " + newResultTimestamp);

            switch (state) {
                case NOT_RUNNING:
                    // We're already finished, so we'll just ignore this
                    break;
                case TAKING_PICTURES:
                    if (shouldUseNewResult()) {
                        if (lastResult != null) {
                            long previousTimestamp = getTimestamp(lastResult);
                            long newTimestamp = getTimestamp(result);

                            if (newTimestamp == previousTimestamp)
                                throw new IllegalArgumentException("New image has the same timestamp as previous one: " + newTimestamp);

                            if (TimestampComparer.compare(newTimestamp, previousTimestamp) < 0)
                                throw new IllegalArgumentException(String.format("New image %d is older than previous one %d", newTimestamp, previousTimestamp));
                        }

                        lastResult = result;
                        checkResult();
                    }

                    break;
                case DETECTING_FACE:
                    // We're already doing detection, so we'll just ignore this
                    break;
                default:
                    throw new IllegalStateException("Unknown state: " + state);
            }
        }

        @Override
        public void onCaptureFailed(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull CaptureFailure failure) {
            super.onCaptureFailed(session, request, failure);
            Log.w(TAG, "Capture failed. Reason: " + FieldConstants.getName(failure.getReason(), "REASON_", CaptureFailure.class));

            if (failure.getReason() == CaptureFailure.REASON_ERROR) {
                // There was an error in the framework. Better restart the camera to get things into a clean state.
                // This once happened on a Pixel 3 XL, and the following system logs were output. This kept happening in a loop, and all captures failed.
                /*
                    E Camera3-OutputStream: getBufferLockedCommon: Stream 1: Can't dequeue next output buffer: Connection timed out (-110)
                    E Camera3-Device: RequestThread: Can't get output buffer, skipping request: Connection timed out (-110)
                 */
                listener.onCameraLost(null);
            }
        }
    };

    private final ImageReader.OnImageAvailableListener imageAvailableCallback = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader) {
            Log.v(TAG, "Image available");

            switch (state) {
                case NOT_RUNNING:
                    Log.w(TAG, "Not running. Dropping image.");
                    // We must always remove the image from the queue so the camera can keep taking pictures.
                    // If we don't remove the image, the camera will no longer send pictures to it.
                    Image image = reader.acquireLatestImage();
                    if (image != null)
                        image.close();

                    break;
                case TAKING_PICTURES:
                    // Sometimes the camera sends us a new image even though we haven't finished with the last one.
                    // We can't acquire the new image because the old one hasn't been closed yet.
                    // We can't drop the new image because we can't acquire it.


                    if (shouldUseNewImage()) {
                        Log.d(TAG, "Acquiring next image");
                        getNextImage();
                    } else {
                        Log.d(TAG, "Ignoring next image");
                    }

                    break;
                case DETECTING_FACE:
                    // We're in the middle of face detection, so we are still holding onto the previous image.
                    // We can't close the previous image since it's in use.
                    // And we can't acquire the new image since the previous one hasn't been closed.
                    // So we'l just have to ignore this and clear the image reader later.
                    Log.v(TAG, "Received new image during face detection. Ignoring.");
                    break;
                default:
                    throw new IllegalStateException("Unknown state: " + state);
            }
        }
    };

    private void getNextImage() {
        boolean hadLastImage = lastImage != null;
        long previousTimestamp = 0;

        if (hadLastImage) {
            previousTimestamp = lastImage.getTimestamp();
            lastImage.close();
            lastImage = null;
        }

        // Grabbing the next image, not the latest one, so we don't skip frames and miss our chance to match a frame up with a capture result
        lastImage = imageReader.acquireNextImage();
        if (lastImage != null) {

            if (hadLastImage) {
                long newTimestamp = lastImage.getTimestamp();

                if (newTimestamp == previousTimestamp)
                    throw new IllegalStateException("New image has the same timestamp as previous one: " + newTimestamp);

                if (TimestampComparer.compare(newTimestamp, previousTimestamp) < 0)
                    throw new IllegalStateException(String.format("New image %d is older than previous one %d", newTimestamp, previousTimestamp));
            }

            String imageFormat = FieldConstants.getName(lastImage.getFormat(), ImageFormat.class);
            Log.v(TAG, String.format("Got image: %d %dx%d (%s)", lastImage.getTimestamp(), lastImage.getWidth(), lastImage.getHeight(), imageFormat));


            checkResult();
        }
    }

    private boolean shouldUseNewImage() {
        if (lastResult == null) {
            // We don't have a capture result yet, so we'll keep using the newest images until we're ready.
            // We cannot hold onto the *oldest* image because its capture result may have already been lost.
            return true;
        }

        // If current image is out-of-date, we'll replace it with the newer one
        return lastImage == null || TimestampComparer.compare(lastImage.getTimestamp(), getTimestamp(lastResult)) < 0;
    }

    private boolean shouldUseNewResult() {
        if (lastImage == null) {
            // We don't have an image yet, so we'll keep using the newest capture result until we're ready.
            // We cannot hold onto the *oldest* capture result because its image may have already been lost.
            return true;
        }

        // If current result is out-of-date, we'll replace it with the newer one
        return lastResult == null || TimestampComparer.compare(getTimestamp(lastResult), lastImage.getTimestamp()) < 0;
    }

    private static long getTimestamp(CaptureResult captureResult) {
        Long timestamp = captureResult.get(CaptureResult.SENSOR_TIMESTAMP);
        assert timestamp != null;
        return timestamp;
    }

    private void checkResult() {
        if (state != State.TAKING_PICTURES)
            throw new IllegalStateException();

        if (isMatch()) {
            Log.v(TAG, "Matched image with capture result. Starting detection.\n" + CameraDump.dump(cameraCharacteristics, lastResult));

            Integer aeState = lastResult.get(CaptureResult.CONTROL_AE_STATE);
            boolean tooDark = aeState == null || aeState == CameraMetadata.CONTROL_AE_STATE_FLASH_REQUIRED;

            if (tooDark)
                // If the camera image is mostly black, we can still extract some detail by boosting the contrast
                // Sometimes this is enough to keep face detection going
                BitmapUtils.autoLevel(lastImage, 1.0f);

            Integer sensorOrientationDegrees = cameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
            assert sensorOrientationDegrees != null; // The docs say this value is always available

            detectionRequest = detector.detectInImage(lastImage, sensorOrientationDegrees, faceDetectorCallback);

            state = State.DETECTING_FACE;
        }
    }

    private boolean exposureFound() {
        if (!isMatch())
            throw new IllegalStateException("Current result+image pair do not match");

        if (firstImageTimestampNanos == 0)
            firstImageTimestampNanos = lastImage.getTimestamp();

        Integer aeState = lastResult.get(CaptureResult.CONTROL_AE_STATE);
        if (aeState == null) {
            // This camera doesn't support monitoring of auto-exposure, so we'll just have to wait a bit and then assume it's adjusted.
            // Note that the exposure only seems to start after the first picture has been taken.
            long nanosSinceFirstImage = lastImage.getTimestamp() - firstImageTimestampNanos;
            long millisSinceFirstImage = nanosSinceFirstImage / 1000 / 1000;

            return millisSinceFirstImage >= 750;
        } else {
            return aeState == CameraMetadata.CONTROL_AE_STATE_CONVERGED || aeState == CameraMetadata.CONTROL_AE_STATE_FLASH_REQUIRED;
        }
    }

    /** Determines whether the current result+image pair are for the same capture. */
    private boolean isMatch() {
        return lastImage != null && lastResult != null && lastImage.getTimestamp() == getTimestamp(lastResult);
    }

    private final FaceDetectionListener faceDetectorCallback = new FaceDetectionListener() {

        @Override
        public void onDetectionResult(FaceDetectionResult result) {
            if (state == State.DETECTING_FACE) {
                detectionRequest = null;

                boolean finishUp;
                if (result.foundFaces()) {
                    finishUp = true;
                    Log.d(TAG, "Detected face");
                } else if (timedOut && hasRetried) {
                    // We should always retry at least once since the first detection might have been performed before auto-exposure finished adjusting
                    finishUp = true;
                    Log.d(TAG, "Timed out");
                } else {
                    finishUp = false;
                    Log.d(TAG, "Detection unsuccessful. Retrying...");
                }

                if (finishUp) {
                    listener.onDetectionComplete(result, lastImage);
                    close();
                } else {
                    hasRetried = true;

                    state = State.TAKING_PICTURES;

                    // We didn't detect the user's face, so we'll discard this frame and try again
                    lastResult = null;

                    // We might already have a new image queued up, so we'll check for it
                    getNextImage();
                }
            }
        }

        @Override
        public void onCancelled() {
            if (state == State.DETECTING_FACE) {
                detectionRequest = null;
                // If we cancelled the request, then we should have already closed down
            }
        }

    };

    private final Runnable onTimeout = new Runnable() {
        @Override
        public void run() {
            if (state != State.NOT_RUNNING) {
                // Not aborting processing immediately since:
                // 1. We need to ensure at least one face detection runs to completion
                // 2. We need to send an image back to the listener on completion
                timedOut = true;
            }
        }
    };

}
