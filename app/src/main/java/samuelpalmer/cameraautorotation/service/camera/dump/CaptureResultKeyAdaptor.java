package samuelpalmer.cameraautorotation.service.camera.dump;

import android.hardware.camera2.CaptureResult;

class CaptureResultKeyAdaptor<TValue> implements KeyAdaptor<TValue> {

    private final CaptureResult captureResult;
    private final CaptureResult.Key<TValue> key;

    public CaptureResultKeyAdaptor(CaptureResult captureResult, CaptureResult.Key<TValue> key) {
        this.captureResult = captureResult;
        this.key = key;
    }

    @Override
    public String getName() {
        return key.getName();
    }

    @Override
    public TValue getValue() {
        return captureResult.get(key);
    }

}
