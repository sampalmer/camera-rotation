package samuelpalmer.cameraautorotation.service.camera;

import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.util.Log;
import android.util.Range;
import android.util.Size;

import androidx.core.content.ContextCompat;

import samuelpalmer.cameraautorotation.service.camera.dump.CameraDump;
import samuelpalmer.cameraautorotation.service.facedetection.FaceDetectionResult;
import samuelpalmer.cameraautorotation.service.facedetection.FaceDetector;
import samuelpalmer.cameraautorotation.service.facedetection.FirebaseMlFaceDetector;
import samuelpalmer.cameraautorotation.utils.FieldConstants;

public class CameraMonitor {

    public void setPredictedUserAngle(float predictedUserAngle) {
        detector.setPredictedUserAngle(predictedUserAngle);
    }

    public interface Listener {
        void onUserRotationChanged(FaceDetectionResult detectionResult, Image image, Integer sensorOrientationDegrees);
    }

    private static final String TAG = CameraMonitor.class.getSimpleName();
    private static final int OUTPUT_FORMAT = ImageFormat.YUV_420_888; // YUV_420_888 is the recommended format for Firebase Vision

    private CameraManager cameraManager;
    private Listener listener;
    private CameraCharacteristics cameraCharacteristics;
    private String cameraId;
    private FaceDetector detector;
    private ImageReader imageReader;
    private Range<Integer> targetFpsRange;
    private CameraMonitorCamera camera;
    private boolean isRunning;
    private boolean started;

    public CameraMonitor(Context context, Listener listener) {
        cameraManager = ContextCompat.getSystemService(context, CameraManager.class);
        this.listener = listener;

        assert cameraManager != null;

        selectBestCamera();

        if (cameraId == null)
            throw new RuntimeException("Couldn't find a suitable camera");

        // Logging camera info
        Log.i(TAG, "Using camera " + cameraId);

        detector = new FirebaseMlFaceDetector();

        Size bestOutputSize = selectBestOutputSize(cameraCharacteristics);

        imageReader = ImageReader.newInstance(bestOutputSize.getWidth(), bestOutputSize.getHeight(), OUTPUT_FORMAT, 1);
        targetFpsRange = selectTargetFpsRange(cameraCharacteristics);

        isRunning = true;
    }

    private void selectBestCamera() {
        // Get cameras
        String[] cameraIds;
        try {
            cameraIds = cameraManager.getCameraIdList();
        } catch (CameraAccessException e) {
            // The documentation doesn't explain why this occurs
            throw new RuntimeException("Failed to get camera list: " + FieldConstants.getName(e.getReason(), CameraAccessException.class), e);
        }

        // Find the best one
        this.cameraCharacteristics = null;
        this.cameraId = null;

        for (String cameraId : cameraIds) {
            CameraCharacteristics characteristics;
            try {
                characteristics = cameraManager.getCameraCharacteristics(cameraId);
            } catch (CameraAccessException e) {
                Log.w(TAG, "Camera disconnected when querying its characteristics. Ignoring since it's probably a removable camera.", e);
                characteristics = null;
            }

            // If the camera was disconnected, then it must be a removable camera, which means it's not what we're looking for
            if (characteristics != null && isSuitableCamera(characteristics)) {
                String dump = CameraDump.dump(characteristics);
                Log.i(TAG, "Found camera " + cameraId + ":\n" + dump);

                // At this stage, the first suitable camera is the "best" one.
                // I expect most phones to only have one front camera.
                // And the official docs say most applications should use the first camera: https://developer.android.com/reference/android/hardware/camera2/CameraMetadata.html#REQUEST_AVAILABLE_CAPABILITIES_LOGICAL_MULTI_CAMERA
                if (cameraCharacteristics == null) {
                    this.cameraCharacteristics = characteristics;
                    this.cameraId = cameraId;
                    // Not breaking the loop here, since I want to dump all suitable cameras' info
                }
            }
        }
    }

    private static boolean isSuitableCamera(CameraCharacteristics characteristics) {
        Integer lensFacing = characteristics.get(CameraCharacteristics.LENS_FACING);
        assert lensFacing != null; // Since it's available on all devices

        if (lensFacing == CameraMetadata.LENS_FACING_FRONT) {
            // Checking for a front-facing lens also eliminates external removable cameras
            Integer hardwareLevel = characteristics.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
            assert hardwareLevel != null; // The docs say this is available on all devices

            // Doing another check to eliminate removable cameras just-in-case
            if (hardwareLevel != CameraMetadata.INFO_SUPPORTED_HARDWARE_LEVEL_EXTERNAL)
                return true;
        }

        return false;
    }

    private static Size selectBestOutputSize(CameraCharacteristics cameraCharacteristics) {
        StreamConfigurationMap streamConfigurationMap = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
        assert streamConfigurationMap != null; // The docs say this is available on all devices

        Size[] outputSizes = streamConfigurationMap.getOutputSizes(OUTPUT_FORMAT);
        if (outputSizes == null)
            throw new RuntimeException("Camera doesn't support YUV_420_888 format. Consider implementing support for JPEG as a fallback.");

        Rect nativeResolution = cameraCharacteristics.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE);
        assert nativeResolution != null; // The documentation says this is always available

        Size bestSize = null;

        for (Size size : outputSizes) {
            // From https://firebase.google.com/docs/ml-kit/android/detect-faces:
            // > Each face you want to detect in an image should be at least 100x100 pixels
            // > For face recognition, you should use an image with dimensions of at least 480x360 pixels.
            // > If you are recognizing faces in real time, capturing frames at this minimum resolution can help reduce latency.
            if (size.getWidth() >= 480 && size.getHeight() >= 360) {
                if (bestSize == null)
                    bestSize = size;
                else {
                    // What we're doing here is multiplying each aspect ratio so that they have a common denominator.
                    // This allows us to compare the aspect ratios using integers, which gives exact results.
                    long bestSizeAspectRatio = (long) bestSize.getWidth() * (long) size.getHeight() * (long) nativeResolution.height();
                    long currentSizeAspectRatio = (long) size.getWidth() * (long) bestSize.getHeight() * (long) nativeResolution.height();
                    long nativeAspectRatio = (long) nativeResolution.width() * (long) bestSize.getHeight() * (long) size.getHeight();

                    long bestSizeFromNative = Math.abs(bestSizeAspectRatio - nativeAspectRatio);
                    long currentSizeFromNative = Math.abs(currentSizeAspectRatio - nativeAspectRatio);

                    // Preferring aspect ratios that are closer to the camera's native aspect ratio
                    // because these probably fit more in the frame.
                    if (currentSizeFromNative < bestSizeFromNative)
                        bestSize = size;
                    else if (currentSizeFromNative == bestSizeFromNative) {
                        // Preferring smaller sizes since face detection is more performant on them
                        if (size.getWidth() * size.getHeight() < bestSize.getWidth() * bestSize.getHeight())
                            bestSize = size;
                    }
                }
            }
        }

        if (bestSize == null)
            throw new RuntimeException("Couldn't find a suitable output size");

        Log.i(TAG, "Using output size " + bestSize.toString());
        return bestSize;
    }

    private static Range<Integer> selectTargetFpsRange(CameraCharacteristics cameraCharacteristics) {
        Range<Integer>[] ranges = cameraCharacteristics.get(CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES);
        assert ranges != null; // The documentation says this is available on all devices

        Range<Integer> bestRange = null;
        for (Range<Integer> range : ranges) {
            if (bestRange == null)
                bestRange = range;
            else {
                int rangeWidth = range.getUpper() - range.getLower();
                int bestRangeWidth = bestRange.getUpper() - bestRange.getLower();

                // Ranges with a greater range of FPS values are better because it gives them more
                // flexibility to adjust the exposure. This in turn allows the camera to better
                // adapt to different lighting levels, which is particularly useful in dark
                // environments since cameras aren't as good as humans at seeing things in the dark.
                if (rangeWidth > bestRangeWidth)
                    bestRange = range;
                else if (rangeWidth == bestRangeWidth) {
                    // If it's a tie, we'll prefer lower FPS ranges since this should allow for
                    // better low-light performance, which I'm more concerned about than high-light.
                    if (range.getLower() < bestRange.getLower())
                        bestRange = range;
                }
            }
        }

        if (bestRange == null)
            throw new RuntimeException("Couldn't find any available FPS ranges. This shouldn't be possible");

        Log.i(TAG, "Using target FPS range " + bestRange);
        return bestRange;
    }

    public void close() {
        if (!isRunning)
            throw new IllegalStateException("Already closed");

        if (started)
            stop();

        isRunning = false;

        targetFpsRange = null;

        imageReader.close();
        imageReader = null;

        detector.close();
        detector = null;

        cameraId = null;
        cameraCharacteristics = null;
        cameraManager = null;
    }

    public void start() {
        if (!isRunning)
            throw new IllegalStateException("Destroyed");

        if (started)
            throw new IllegalStateException("Already started");

        camera = new CameraMonitorCamera(cameraManager, cameraId, imageReader, detector, cameraCharacteristics, targetFpsRange, listener);
        started = true;
    }

    public void stop() {
        if (!isRunning)
            throw new IllegalStateException("Destroyed");

        if (!started)
            throw new IllegalStateException("Already stopped");

        if (camera != null) {
            camera.close();
            camera = null;
        }

        started = false;
    }

    public boolean isStarted() {
        return started;
    }
}
