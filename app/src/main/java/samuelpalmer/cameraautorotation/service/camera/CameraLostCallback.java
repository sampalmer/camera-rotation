package samuelpalmer.cameraautorotation.service.camera;

import android.hardware.camera2.CameraAccessException;

import androidx.annotation.Nullable;

interface CameraLostCallback {

    void onCameraLost(@Nullable CameraAccessException e);

}
