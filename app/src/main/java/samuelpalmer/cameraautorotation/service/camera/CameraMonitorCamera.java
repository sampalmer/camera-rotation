package samuelpalmer.cameraautorotation.service.camera;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.media.ImageReader;
import android.util.Log;
import android.util.Range;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import samuelpalmer.cameraautorotation.service.facedetection.FaceDetector;
import samuelpalmer.cameraautorotation.utils.FieldConstants;

public class CameraMonitorCamera {

    private static final String TAG = CameraMonitorCamera.class.getSimpleName();

    private enum CameraState {
        CLOSED,
        OPENING,
        ACQUIRED,
        CLOSING,
    }

    private final CameraManager cameraManager;
    private final String cameraId;
    private final ImageReader imageReader;
    private final FaceDetector detector;
    private final CameraCharacteristics cameraCharacteristics;
    private final Range<Integer> targetFpsRange;
    private final CameraMonitor.Listener listener;
    private final TorchMonitor torchMonitor;

    private boolean isRunning;
    private boolean isCameraAvailable;
    private CameraState cameraState;
    private CameraStateCallback cameraStateCallback;
    private CameraDevice camera;
    private CameraMonitorSession session;

    public CameraMonitorCamera(CameraManager cameraManager, String cameraId, ImageReader imageReader, FaceDetector detector, CameraCharacteristics cameraCharacteristics, Range<Integer> targetFpsRange, CameraMonitor.Listener listener) {
        this.cameraManager = cameraManager;
        this.cameraId = cameraId;
        this.imageReader = imageReader;
        this.detector = detector;
        this.cameraCharacteristics = cameraCharacteristics;
        this.targetFpsRange = targetFpsRange;
        this.listener = listener;

        isRunning = true;
        isCameraAvailable = true; // Assuming the camera is available for the time being since we don't know. We'll find out whether it's really available after trying to open it.
        cameraState = CameraState.CLOSED;

        cameraManager.registerAvailabilityCallback(availabilityCallback, null);
        torchMonitor = new TorchMonitor(cameraManager, torchCallback);

        tryOpenCamera();
    }

    public void close() {
        if (!isRunning)
            throw new IllegalStateException("Already closed");

        isRunning = false;

        switch (cameraState) {
            case CLOSED:
                // It's already closed, so no need to do anything here
                break;
            case OPENING:
                // The camera is already opening, and we can't stop it. So we'll wait for it to
                // open and then close it again.
                break;
            case ACQUIRED:
                beginClosingCamera();
                break;
            case CLOSING:
                // It's already closing, so no need to do anything here
                break;
            default:
                throw new IllegalStateException("Unknown camera state: " + cameraState);
        }

        // Not cleaning up the camera-specific bits here since that will happen when the camera finishes closing
        torchMonitor.close();
        cameraManager.unregisterAvailabilityCallback(availabilityCallback);
        isCameraAvailable = false;
    }

    private final CameraManager.AvailabilityCallback availabilityCallback = new CameraManager.AvailabilityCallback() {
        @Override
        public void onCameraAvailable(@NonNull String cameraId) {
            super.onCameraAvailable(cameraId);

            if (isRunning) {
                Log.i(TAG, String.format("Camera %s available", cameraId));

                // It doesn't matter which camera became available. Sometimes the target camera will be silently unavailable until a different camera becomes available.
                isCameraAvailable = true;
                tryOpenCamera();
            }
        }

        @Override
        public void onCameraUnavailable(@NonNull String cameraId) {
            super.onCameraUnavailable(cameraId);

            if (isRunning) {
                Log.i(TAG, String.format("Camera %s unavailable", cameraId));
                // Not checking the camera id since it isn't this callback isn't a reliable indicator of whether the target camera is actually available
            }
        }
    };

    private final TorchMonitor.Listener torchCallback = new TorchMonitor.Listener() {
        @Override
        public void onChanged() {
            if (isRunning)
                tryOpenCamera();
        }
    };

    private class CameraStateCallback extends CameraDevice.StateCallback {

        // TODO: Relax this. If the OS does stuff with the camera we weren't expecting, we need to adapt to it rather than ignore it.
        private boolean isCallbackActive = true;

        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            if (isCallbackActive && cameraState == CameraState.OPENING) {
                Log.i(TAG, "Camera opened");
                CameraMonitorCamera.this.camera = camera;

                if (isRunning) {
                    Log.i(TAG, "Creating capture session");
                    try {
                        CameraMonitorCamera.this.session = new CameraMonitorSession(imageReader, detector, camera, cameraCharacteristics, targetFpsRange, new CameraLostCallback() {
                            @Override
                            public void onCameraLost(@Nullable CameraAccessException e) {
                                if (cameraState == CameraState.OPENING || cameraState == CameraState.ACQUIRED) {
                                    String reason;
                                    if (e != null)
                                        reason = FieldConstants.getName(e.getReason(), CameraAccessException.class);
                                    else
                                        reason = null;

                                    Log.i(TAG, "Camera session died: " + reason);

                                    isCameraAvailable = false;

                                    // The camera is currently in use by something else, so we'll have to wait for it
                                    beginClosingCamera();
                                }
                            }
                        }, listener);
                        cameraState = CameraState.ACQUIRED;
                    } catch (CameraAccessException e) {
                        Log.i(TAG, "Creating capture session failed: " + FieldConstants.getName(e.getReason(), CameraAccessException.class));

                        isCameraAvailable = false;

                        // The camera is currently in use by something else, so we'll have to wait for it
                        beginClosingCamera();
                    }
                } else {
                    // This can happen if the client has already closed us down, but the camera
                    // was still opening at the time. So just close the camera and let things shut down.
                    beginClosingCamera();
                }
            }
        }

        @Override
        public void onClosed(@NonNull CameraDevice camera) {
            super.onClosed(camera);

            if (isCallbackActive && cameraState == CameraState.CLOSING) {
                Log.i(TAG, "Camera closed");
                cleanUpCamera();
            }
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            if (isCallbackActive) {
                if (cameraState == CameraState.OPENING || cameraState == CameraState.ACQUIRED) {
                    Log.i(TAG, "Camera disconnected");

                    // Storing the camera reference since we might not have got as far as onOpened()
                    CameraMonitorCamera.this.camera = camera;

                    beginClosingCamera();
                }
            }
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            if (isCallbackActive)
                if (cameraState == CameraState.OPENING || cameraState == CameraState.ACQUIRED) {
                    Log.w(TAG, "Camera got error " + FieldConstants.getName(error, "ERROR_", CameraDevice.StateCallback.class));

                    // TODO: Add specific handling for the different possible error codes
                    isCameraAvailable = false;

                    // Storing the camera reference since we might not have got as far as onOpened()
                    CameraMonitorCamera.this.camera = camera;

                    beginClosingCamera();
                }
        }

    }

    private void tryOpenCamera() {
        // Not opening the camera if a torch is in use since we don't want to interfere with what the user is doing.
        // The OS prioritises camera use over torch use, so we need to manually keep an eye on the torch to ensure we don't interfere.
        if (isRunning && isCameraAvailable && torchMonitor.getTorchUse() == TorchMonitor.TorchUse.NOT_IN_USE && cameraState == CameraState.CLOSED) {
            Log.i(TAG, "Opening camera");
            cameraState = CameraState.OPENING;
            cameraStateCallback = new CameraStateCallback();

            try {
                cameraManager.openCamera(cameraId, cameraStateCallback, null);
            } catch (SecurityException e) {
                // TODO: Gracefully handle permission errors
                throw new RuntimeException("Don't have camera permission");
            } catch (CameraAccessException e) {
                Log.i(TAG, "Opening camera failed: " + FieldConstants.getName(e.getReason(), CameraAccessException.class));

                // TODO: Handle other error codes here
                // The camera is currently in use by something else, so we'll have to wait for it
                isCameraAvailable = false;

                cleanUpCamera();
            }
        }
    }

    private void beginClosingCamera() {
        if (cameraState != CameraState.OPENING && cameraState != CameraState.ACQUIRED)
            throw new IllegalStateException("Cannot close camera from state " + cameraState);

        Log.i(TAG, "Closing camera");

        // We might not have created a session if this closure is due to disconnection prior to onOpened()
        if (session != null) {
            session.close();
            session = null;
        }

        camera.close();
        cameraState = CameraState.CLOSING;
    }

    /**
     * Resets the state to {@link CameraState#CLOSED} status. This should only be called when the underlying camera has already been closed.
     */
    private void cleanUpCamera() {
        if (cameraState == CameraState.CLOSED)
            throw new IllegalStateException("Camera state is already cleaned up");

        // We might not have created a session if this closure is due to disconnection prior to onOpened()
        if (session != null) {
            session.close();
            session = null;
        }

        if (CameraMonitorCamera.this.camera != null)
            CameraMonitorCamera.this.camera = null;

        cameraStateCallback.isCallbackActive = false;
        cameraStateCallback = null;

        cameraState = CameraState.CLOSED;

        // The camera might have since become available once again
        tryOpenCamera();
    }

}
