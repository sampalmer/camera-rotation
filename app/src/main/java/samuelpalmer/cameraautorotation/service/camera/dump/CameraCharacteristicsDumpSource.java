package samuelpalmer.cameraautorotation.service.camera.dump;

import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.os.Build;
import android.util.Range;
import android.util.Size;
import android.util.SizeF;

import samuelpalmer.cameraautorotation.utils.FieldConstants;

class CameraCharacteristicsDumpSource extends CameraDumpSource {

    private final CameraCharacteristics cameraCharacteristics;

    CameraCharacteristicsDumpSource(CameraCharacteristics cameraCharacteristics) {
        super(cameraCharacteristics);
        this.cameraCharacteristics = cameraCharacteristics;

        append(wrap(CameraCharacteristics.LENS_FACING), lensFacing -> getValueName(lensFacing, "LENS_FACING_"));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
            append(wrap(CameraCharacteristics.INFO_VERSION));

        append(wrap(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL), hardwareLevel -> getValueName(hardwareLevel, "INFO_SUPPORTED_HARDWARE_LEVEL_"));
        appendIntArray(wrap(CameraCharacteristics.CONTROL_AE_AVAILABLE_MODES), aeMode -> getValueName(aeMode, "CONTROL_AE_MODE_"));
        appendArray(wrap(CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES), Range::toString);
        appendRange(wrap(CameraCharacteristics.CONTROL_AE_COMPENSATION_RANGE), this::formatExposureCompensation);
        append(wrap(CameraCharacteristics.CONTROL_AE_COMPENSATION_STEP));

        appendIntArray(wrap(CameraCharacteristics.CONTROL_AWB_AVAILABLE_MODES), awbMode -> getValueName(awbMode, "CONTROL_AWB_MODE_"));
        append(wrap(CameraCharacteristics.CONTROL_MAX_REGIONS_AWB));

        append(wrap(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE), Rect::toShortString);
        appendIntArray(wrap(CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES), afMode -> getValueName(afMode, "CONTROL_AF_MODE_"));
        appendIntArray(wrap(CameraCharacteristics.CONTROL_AVAILABLE_MODES), mode -> getValueName(mode, "CONTROL_MODE_"));
        appendIntArray(wrap(CameraCharacteristics.CONTROL_AVAILABLE_SCENE_MODES), sceneMode -> getValueName(sceneMode, "CONTROL_SCENE_MODE_"));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            append(wrap(CameraCharacteristics.CONTROL_POST_RAW_SENSITIVITY_BOOST_RANGE));

        append(wrap(CameraCharacteristics.FLASH_INFO_AVAILABLE));
        appendIntArray(wrap(CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES), capability -> getValueName(capability, "REQUEST_AVAILABLE_CAPABILITIES_"));
        appendRange(wrap(CameraCharacteristics.SENSOR_INFO_EXPOSURE_TIME_RANGE), exposureTimeNanos -> exposureTimeNanos + "ns");
        append(wrap(CameraCharacteristics.SENSOR_INFO_MAX_FRAME_DURATION), maxFrameDuration -> maxFrameDuration + "ns");
        append(wrap(CameraCharacteristics.SENSOR_INFO_SENSITIVITY_RANGE));
        append(wrap(CameraCharacteristics.SENSOR_MAX_ANALOG_SENSITIVITY));
        appendIntArray(wrap(CameraCharacteristics.STATISTICS_INFO_AVAILABLE_FACE_DETECT_MODES), faceDetectMode -> getValueName(faceDetectMode, "STATISTICS_FACE_DETECT_MODE_"));
        append(wrap(CameraCharacteristics.STATISTICS_INFO_MAX_FACE_COUNT));
        append(wrap(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP), CameraCharacteristicsDumpSource::formatStreamConfiguration);
        appendIntArray(wrap(CameraCharacteristics.LENS_INFO_AVAILABLE_OPTICAL_STABILIZATION), oisMode -> getValueName(oisMode, "LENS_OPTICAL_STABILIZATION_MODE_"));
        appendFloatArray(wrap(CameraCharacteristics.LENS_INFO_AVAILABLE_FOCAL_LENGTHS), focalLengthMm -> focalLengthMm + "mm");
        append(wrap(CameraCharacteristics.SENSOR_INFO_PHYSICAL_SIZE), sensorSizeMm -> sensorSizeMm + "mm");
        appendFloatArray(wrap(CameraCharacteristics.LENS_INFO_AVAILABLE_APERTURES), aperture -> "f/" + aperture);

        // Field of view calculated from https://stackoverflow.com/a/37916854/238753
        float[] maxFocus = cameraCharacteristics.get(CameraCharacteristics.LENS_INFO_AVAILABLE_FOCAL_LENGTHS);
        assert maxFocus != null; // The docs say this is always available

        SizeF size = cameraCharacteristics.get(CameraCharacteristics.SENSOR_INFO_PHYSICAL_SIZE);
        assert size != null; // The docs say this is always available

        float w = size.getWidth();
        float h = size.getHeight();
        double horizonalAngle = 2 * Math.atan(w / (maxFocus[0] * 2)) * 180 / Math.PI;
        double verticalAngle = 2 * Math.atan(h / (maxFocus[0] * 2)) * 180 / Math.PI;

        append("samuelpalmer.cameraautorotation.horizontalFov", Math.round(horizonalAngle) + "°");
        append("samuelpalmer.cameraautorotation.verticalFov", Math.round(verticalAngle) + "°");
    }

    private <TValue> KeyAdaptor<TValue> wrap(CameraCharacteristics.Key<TValue> key) {
        return new CameraCharacteristicsKeyAdaptor<>(cameraCharacteristics, key);
    }

    private static String formatStreamConfiguration(StreamConfigurationMap streamConfig) {
        StringBuilder result = new StringBuilder();
        int[] formats = streamConfig.getOutputFormats();

        for (int i = 0; i < formats.length; ++i) {
            int format = formats[i];
            String formatName = FieldConstants.getName(format, PixelFormat.class, ImageFormat.class);
            result
                    .append(formatName)
                    .append(" = [");

            Size[] sizes = streamConfig.getOutputSizes(format);
            for (int j = 0; j < sizes.length; ++j) {
                Size size = sizes[j];
                result.append(size);

                if (j != sizes.length - 1)
                    result.append(", ");
            }

            result.append("]");
            if (i != formats.length - 1)
                result.append(", ");
        }

        return result.toString();
    }

}
