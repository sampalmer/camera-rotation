package samuelpalmer.cameraautorotation.service.camera.dump;

import android.hardware.camera2.CaptureRequest;

class CaptureRequestKeyAdaptor<TValue> implements KeyAdaptor<TValue> {

    private final CaptureRequest request;
    private final CaptureRequest.Key<TValue> key;

    public CaptureRequestKeyAdaptor(CaptureRequest request, CaptureRequest.Key<TValue> key) {
        this.request = request;
        this.key = key;
    }

    @Override
    public String getName() {
        return key.getName();
    }

    @Override
    public TValue getValue() {
        return request.get(key);
    }

}
