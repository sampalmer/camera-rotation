package samuelpalmer.cameraautorotation.service.camera.dump;

/**
 * Provides a generic way to use the various <code>Key</code> classes in the
 * {@link android.hardware.camera2} APIs
 */
interface KeyAdaptor<TValue> {
    /**
     * Identifies what the key for display purposes. Does not include the key's value.
     */
    String getName();

    /**
     * Gets the key's value associated with this class's record
     */
    TValue getValue();
}
