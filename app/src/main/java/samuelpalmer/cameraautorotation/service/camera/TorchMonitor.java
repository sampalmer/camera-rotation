package samuelpalmer.cameraautorotation.service.camera;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import samuelpalmer.cameraautorotation.utils.FieldConstants;

/**
 * Keeps track of whether any torch is currently in use. This is important because any torches that
 * are in use will be automatically switched off when we run the camera.
 */
public class TorchMonitor {

    public interface Listener {
        void onChanged();
    }

    public enum TorchUse {
        /**
         * We currently don't know whether a torch is in use
         */
        UNKNOWN,

        /**
         * A torch is in use
         */
        IN_USE,

        /**
         * No torch is in use
         */
        NOT_IN_USE
    }

    private static final String TAG = TorchMonitor.class.getSimpleName();

    private enum TorchStatus {

        /**
         * We know this is a torch, but we don't yet know whether it's off or on
         */
        UNKNOWN,

        OFF,

        ON,

    }

    private final CameraManager cameraManager;
    private final Listener listener;
    private final Map<String, TorchStatus> torches;

    private boolean isRunning;
    private TorchUse torchUse;

    public TorchMonitor(CameraManager cameraManager, Listener listener) {
        this.listener = listener;
        this.cameraManager = cameraManager;
        this.torches = new HashMap<>();

        isRunning = true;

        cameraManager.registerAvailabilityCallback(availabilityCallback, null);
        cameraManager.registerTorchCallback(torchCallback, null);

        tryFindTorches();
        somethingChanged(false);
    }

    public void close() {
        if (!isRunning)
            throw new IllegalStateException("Already closed");

        cameraManager.unregisterAvailabilityCallback(availabilityCallback);
        cameraManager.unregisterTorchCallback(torchCallback);

        isRunning = false;
    }

    /**
     * Gets whether any camera on the device has its flash unit on in torch mode.
     */
    public TorchUse getTorchUse() {
        if (!isRunning)
            throw new IllegalStateException("Not running");

        return torchUse;
    }

    private void tryFindTorches() {
        // Get the list of cameras
        String[] cameraIds;
        try {
            cameraIds = cameraManager.getCameraIdList();
        } catch (CameraAccessException e) {
            // The cameras are completely unavailable, so we won't be able to determine which ones have torches until they become available
            String reasonName = FieldConstants.getName(e.getReason(), CameraAccessException.class);
            Log.i(TAG, String.format("Cannot identify torches due to %s. Waiting for availability...", reasonName));
            return;
        }

        // Check which cameras have torches
        for (String cameraId : cameraIds) {
            // We only need to add torches that we don't already know about.
            // Plus, we shouldn't be replacing the state of an existing torch since we'll be losing information.
            if (!torches.containsKey(cameraId)) {
                try {
                    // TODO: Cache whether each camera has a flash so we don't keep looking it up each time it's unavailable/available
                    CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(cameraId);

                    //noinspection ConstantConditions Since the docs say this is always available
                    boolean hasFlashUnit = characteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE);
                    if (hasFlashUnit) {
                        Log.v(TAG, "Found torch: " + cameraId);
                        torches.put(cameraId, TorchStatus.UNKNOWN);
                    }
                } catch (CameraAccessException e) {
                    // This camera was disconnected. If it was physically removed from the device, then it is no longer relevant.
                    // If it is just in use by another app then it is also no longer relevant since its torch mode will be off (since torches cannot be used while cameras are in use).
                    String reasonName = FieldConstants.getName(e.getReason(), CameraAccessException.class);
                    Log.d(TAG, String.format("Cannot check whether camera %s has torch due to %s. Ignoring since it is no longer relevant.", cameraId, reasonName));
                }
            }
        }

        // Now that we know what torches exist, we're ready to monitor their statuses
        Log.d(TAG, String.format("Found %d torches", torches.size()));
    }

    private final CameraManager.AvailabilityCallback availabilityCallback =  new CameraManager.AvailabilityCallback() {

        @Override
        public void onCameraAvailable(@NonNull String cameraId) {
            super.onCameraAvailable(cameraId);

            if (isRunning) {
                // We don't know if we successfully enumerated the torches at startup, so now might be a good time to retry.
                Log.d(TAG, String.format("Camera %s is available. Reattempting to find torches in case we missed any at startup.", cameraId));

                tryFindTorches();

                somethingChanged(true);
            }
        }

        @Override
        public void onCameraUnavailable(@NonNull String cameraId) {
            super.onCameraUnavailable(cameraId);

            if (isRunning) {
                Log.d(TAG, String.format("Camera %s is unavailable. No longer tracking it.", cameraId));

                // A few scenarios:
                // 1. Camera is not a torch, so there's nothing to remove
                // 2. Camera was physically disconnected from the device, so it's no longer relevant
                // 3. Camera is now in use, so we don't need to track it since the OS won't let anything use a torch while a camera is in use
                torches.remove(cameraId);

                somethingChanged(true);
            }
        }

    };

    private final CameraManager.TorchCallback torchCallback = new CameraManager.TorchCallback() {

        @Override
        public void onTorchModeUnavailable(@NonNull String cameraId) {
            super.onTorchModeUnavailable(cameraId);

            if (isRunning) {
                Log.d(TAG, String.format("Torch %s is unavailable. No longer tracking it.", cameraId));

                // A few scenarios:
                // 1. Torch was physically disconnected from the device, so it's no longer relevant
                // 2. Torch's camera is now in use, so we don't need to track it since the OS won't let anything use a torch while a camera is in use
                torches.remove(cameraId);

                somethingChanged(true);
            }
        }

        @Override
        public void onTorchModeChanged(@NonNull String cameraId, boolean enabled) {
            super.onTorchModeChanged(cameraId, enabled);

            if (isRunning) {
                Log.d(TAG, String.format("Torch %s is %s", cameraId, enabled ? "on" : "off"));

                torches.put(cameraId, enabled ? TorchStatus.ON : TorchStatus.OFF);

                somethingChanged(true);
            }
        }

    };

    private void somethingChanged(boolean reportToListener) {
        TorchUse oldTorchUse = torchUse;

        if (torches.values().contains(TorchStatus.ON)) {
            // If any torch is turned on, then we can safely conclude that a torch is "in use"
            torchUse = TorchUse.IN_USE;
        } else if (torches.values().contains(TorchStatus.UNKNOWN)) {
            // At least one torch's status hasn't yet been determined, so we'll need to wait before giving a conclusive answer
            torchUse = TorchUse.UNKNOWN;
        } else {
            // We'll default to "not in use" because either:
            // 1. All torches are off or unavailable. Off means not in use. Unavailable means the torch's camera is in use, meaning the torch itself cannot be switched on.
            // 2. No torches exist, so there's nothing to worry about
            torchUse = TorchUse.NOT_IN_USE;
        }

        if (torchUse != oldTorchUse) {
            Log.i(TAG, "Torch use is " + torchUse);

            if (reportToListener)
                listener.onChanged();
        }
    }

}
