package samuelpalmer.cameraautorotation.service.camera;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.util.Log;
import android.util.Range;
import android.view.Surface;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Collections;
import java.util.List;

import samuelpalmer.cameraautorotation.service.facedetection.FaceDetectionResult;
import samuelpalmer.cameraautorotation.service.facedetection.FaceDetector;

class CameraMonitorSession {

    private static final String TAG = CameraMonitorSession.class.getSimpleName();
    private static final int CAPTURE_INTERVAL_MILLIS = 500;

    private final Handler handler;
    private final ImageReader imageReader;
    private final FaceDetector detector;
    private final CameraDevice camera;
    private final CameraCharacteristics cameraCharacteristics;
    private final Range<Integer> targetFpsRange;
    private final CameraLostCallback cameraLostCallback;
    private final CameraMonitor.Listener listener;

    private boolean isRunning;
    private CameraCaptureSession captureSession;
    private CameraMonitorRequest request;

    public CameraMonitorSession(ImageReader imageReader, FaceDetector detector, CameraDevice camera, CameraCharacteristics cameraCharacteristics, Range<Integer> targetFpsRange, CameraLostCallback cameraLostCallback, CameraMonitor.Listener listener) throws CameraAccessException {
        this.imageReader = imageReader;
        this.detector = detector;
        this.camera = camera;
        this.cameraCharacteristics = cameraCharacteristics;
        this.targetFpsRange = targetFpsRange;
        this.cameraLostCallback = cameraLostCallback;
        this.listener = listener;
        handler = new Handler();

        List<Surface> outputs = Collections.singletonList(imageReader.getSurface());

        camera.createCaptureSession(outputs, captureSessionCallback, null);

        isRunning = true;
    }

    public void close() {
        ensureRunning();

        isRunning = false;

        if (request != null) {
            request.close();
            request = null;
        }

        captureSession = null;

        handler.removeCallbacks(tick);
    }

    private final Runnable tick = new Runnable() {
        @Override
        public void run() {
            ensureRunning();

            if (isRunning && camera != null && captureSession != null && request == null)
                sendCaptureRequest();
        }
    };

    private final CameraCaptureSession.StateCallback captureSessionCallback = new CameraCaptureSession.StateCallback() {
        @Override
        public void onConfigured(@NonNull CameraCaptureSession session) {
            if (isRunning) {
                Log.i(TAG, "Camera session configured");

                CameraMonitorSession.this.captureSession = session;

                if (camera != null && request == null)
                    sendCaptureRequest();
            }
        }

        @Override
        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
            if (isRunning)
                // This means bad parameters were used when creating the session
                throw new RuntimeException("Camera capture session configuration failed");
        }

        @Override
        public void onClosed(@NonNull CameraCaptureSession session) {
            super.onClosed(session);

            if (isRunning) {
                Log.i(TAG, "Camera session closed");

                // We get notified that a session was closed after it was already replaced with a newly-configured session, so need to filter these out
                if (CameraMonitorSession.this.captureSession == session)
                    captureSession = null;
            }
        }

        @Override
        public void onCaptureQueueEmpty(@NonNull CameraCaptureSession session) {
            super.onCaptureQueueEmpty(session);

            if (isRunning)
                Log.v(TAG, "Camera session capture queue empty");
        }

        @Override
        public void onActive(@NonNull CameraCaptureSession session) {
            super.onActive(session);

            if (isRunning)
                Log.v(TAG, "Camera session capture queue active");
        }

        @Override
        public void onReady(@NonNull CameraCaptureSession session) {
            super.onReady(session);

            if (isRunning)
                Log.v(TAG, "Camera session capture queue ready");
        }

        @Override
        public void onSurfacePrepared(@NonNull CameraCaptureSession session, @NonNull Surface surface) {
            super.onSurfacePrepared(session, surface);

            if (isRunning)
                Log.v(TAG, "Camera session capture queue surface prepared");
        }

    };

    private void sendCaptureRequest() {
        ensureRunning();

        if (captureSession == null)
            throw new IllegalStateException("Cannot send capture request because capture session is not open");

        Log.v(TAG, "Sending capture request");

        try {
            request = new CameraMonitorRequest(imageReader, detector, camera, captureSession, cameraCharacteristics, targetFpsRange, new CameraMonitorRequest.Listener() {

                @Override
                public void onDetectionComplete(FaceDetectionResult detectionResult, Image image) {
                    request = null; // Request is complete so is now closed


                    Integer sensorOrientationDegrees = cameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
                    assert sensorOrientationDegrees != null; // The docs say this value is always available

                    listener.onUserRotationChanged(detectionResult, image, sensorOrientationDegrees);

                    if (captureSession != null)
                        handler.postDelayed(tick, CAPTURE_INTERVAL_MILLIS);
                }

                @Override
                public void onCameraLost(@Nullable CameraAccessException e) {
                    request.close();
                    request = null;

                    cameraLostCallback.onCameraLost(e);
                }

            });
        } catch (CameraAccessException e) {
            cameraLostCallback.onCameraLost(e);
        }
    }

    private void ensureRunning() {
        if (!isRunning)
            throw new IllegalStateException("Not running");
    }

}
