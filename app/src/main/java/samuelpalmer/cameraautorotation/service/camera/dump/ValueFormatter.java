package samuelpalmer.cameraautorotation.service.camera.dump;

@FunctionalInterface
interface ValueFormatter<TValue> {
    String format(TValue value);
}
