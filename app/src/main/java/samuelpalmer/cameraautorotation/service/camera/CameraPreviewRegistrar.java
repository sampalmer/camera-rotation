package samuelpalmer.cameraautorotation.service.camera;

import android.media.Image;
import android.util.Log;

import java.util.HashSet;
import java.util.Set;

import samuelpalmer.cameraautorotation.service.facedetection.FaceDetectionResult;

public abstract class CameraPreviewRegistrar {

    private static final String TAG = CameraPreviewRegistrar.class.getSimpleName();

    private static final Set<Listener> currentPreviews = new HashSet<>();

    public interface Listener {
        void onPreviewAvailable(Image image, int cameraOrientationDegrees, FaceDetectionResult detectionResult, int userRotation);
        void onPreviewUnavailable();
    }

    public static void addPreview(Listener newPreview) {
        boolean wasAdded = currentPreviews.add(newPreview);
        if (!wasAdded)
            throw new IllegalStateException("This preview is already added");

        Log.i(TAG, "Preview surface added");
    }

    public static void removePreview(Listener previewToRemove) {
        boolean wasRemoved = currentPreviews.remove(previewToRemove);
        if (!wasRemoved)
            throw new IllegalArgumentException("This preview was not present");

        Log.i(TAG, "Preview surface removed");
    }

    public static void update(Image image, int cameraOrientationDegrees, FaceDetectionResult detectionResult, int userRotation) {
        for (Listener preview : currentPreviews)
            preview.onPreviewAvailable(image, cameraOrientationDegrees, detectionResult, userRotation);
    }

    public static void clear() {
        for (Listener preview : currentPreviews)
            preview.onPreviewUnavailable();
    }

}
