package samuelpalmer.cameraautorotation.service.camera.dump;

import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;

public abstract class CameraDump {

    public static String dump(CameraCharacteristics cameraCharacteristics) {
        return new CameraCharacteristicsDumpSource(cameraCharacteristics).dump();
    }

    public static String dump(CameraCharacteristics cameraCharacteristics, CaptureRequest request) {
        return new CaptureRequestDumpSource(cameraCharacteristics, request).dump();
    }

    public static String dump(CameraCharacteristics cameraCharacteristics, CaptureResult result) {
        return new CaptureResultDumpSource(cameraCharacteristics, result).dump();
    }

}
