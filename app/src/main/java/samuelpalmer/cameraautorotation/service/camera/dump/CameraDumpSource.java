package samuelpalmer.cameraautorotation.service.camera.dump;

import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraMetadata;
import android.text.TextUtils;
import android.util.Range;
import android.util.Rational;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Locale;

import samuelpalmer.cameraautorotation.utils.FieldConstants;

abstract class CameraDumpSource {

    private final CameraCharacteristics cameraCharacteristics;
    private final StringBuilder resultBuilder;

    /**
     * In your constructor, determine what to output by calling the various <code>append</code>
     * methods.
     */
    public CameraDumpSource(CameraCharacteristics cameraCharacteristics) {
        this.cameraCharacteristics = cameraCharacteristics;
        resultBuilder = new StringBuilder();
    }

    /**
     * Returns the result of the formatting applied in the constructor
     */
    public String dump() {
        return resultBuilder.toString();
    }

    /**
     * Adds the key-value pair to the end of the resulting string.
     * Formats the key's array value as a comma-separated string.
     * Each element in the array is formatted using the given formatter.
     *
     * @param elementFormatter Used to format each element in the key's array value
     */
    void appendIntArray(KeyAdaptor<int[]> key, ValueFormatter<Integer> elementFormatter) {
        append(key, values -> {
            String[] formattedValues = new String[values.length];

            for (int i = 0; i < values.length; ++i) {
                int value = values[i];
                String formattedValue = elementFormatter.format(value);
                formattedValues[i] = formattedValue;
            }

            return TextUtils.join(", ", formattedValues);
        });
    }

    /**
     * Adds the key-value pair to the end of the resulting string.
     * Formats the key's array value as a comma-separated string.
     * Each element in the array is formatted using the given formatter.
     *
     * @param elementFormatter Used to format each element in the key's array value
     */
    void appendFloatArray(KeyAdaptor<float[]> key, ValueFormatter<Float> elementFormatter) {
        append(key, values -> {
            String[] formattedValues = new String[values.length];

            for (int i = 0; i < values.length; ++i) {
                float value = values[i];
                String formattedValue = elementFormatter.format(value);
                formattedValues[i] = formattedValue;
            }

            return TextUtils.join(", ", formattedValues);
        });
    }

    /**
     * Adds the key-value pair to the end of the resulting string.
     * Formats the key's array value as a comma-separated string.
     * Each element in the array is formatted using the given formatter.
     *
     * @param elementFormatter Used to format each element in the key's array value
     */
    <TElement> void appendArray(@SuppressWarnings("SameParameterValue") KeyAdaptor<TElement[]> key, ValueFormatter<TElement> elementFormatter) {
        append(key, values -> {
            String[] formattedValues = new String[values.length];

            for (int i = 0; i < values.length; ++i) {
                TElement value = values[i];
                String formattedValue = elementFormatter.format(value);
                formattedValues[i] = formattedValue;
            }

            return TextUtils.join(", ", formattedValues);
        });
    }

    /**
     * Adds the key-value pair to the end of the resulting string.
     * Formats the key's value using {@link Object#toString()}.
     */
    <TValue> void append(KeyAdaptor<TValue> key) {
        append(key, null);
    }

    /**
     * Adds the key-value pair to the end of the resulting string.
     * Formats the key's value using the given formatter.
     *
     * @param valueFormatter Used to format the key's value in the result
     */
    <TValue> void append(KeyAdaptor<TValue> key, @Nullable ValueFormatter<TValue> valueFormatter) {
        TValue value = key.getValue();
        String formattedValue;

        if (value == null)
            formattedValue = null;
        else if (valueFormatter == null)
            formattedValue = value.toString();
        else
            formattedValue = valueFormatter.format(value);

        append(key.getName(), formattedValue);
    }

    void append(String keyName, String formattedValue) {
        resultBuilder
                .append(keyName)
                .append(": ");

        resultBuilder
                .append(formattedValue)
                .append('\n');
    }

    <TBound extends Comparable<? super TBound>> void appendRange(KeyAdaptor<Range<TBound>> key, @NonNull ValueFormatter<TBound> boundFormatter) {
        append(key, range -> {
            String formattedLower = boundFormatter.format(range.getLower());
            String formattedUpper = boundFormatter.format(range.getUpper());

            // Not putting the formatted text back into a Range since it might crash when trying to compare/validate them.
            return String.format("[%s, %s]", formattedLower, formattedUpper);
        });
    }

    /**
     * Shorthand for <code>FieldConstants.getName(value, prefix, CameraMetadata.class)</code>
     */
    static String getValueName(Object value, String prefix) {
        return FieldConstants.getName(value, prefix, CameraMetadata.class);
    }

    /**
     * Formats the given exposure compensation amount as an Exposure Value (EV)
     */
    String formatExposureCompensation(Integer exposureCompensation) {
        Rational aeCompensationStep = cameraCharacteristics.get(CameraCharacteristics.CONTROL_AE_COMPENSATION_STEP);
        assert aeCompensationStep != null; // The documentation says this is always available

        long exposureValue = Math.round(exposureCompensation * aeCompensationStep.doubleValue());
        return String.format(Locale.UK, "%+d", exposureValue);
    }

}
