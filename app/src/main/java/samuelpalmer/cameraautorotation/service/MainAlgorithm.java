package samuelpalmer.cameraautorotation.service;

import android.content.Context;
import android.media.Image;
import android.os.Handler;
import android.util.Log;
import android.view.Surface;

import samuelpalmer.cameraautorotation.service.camera.CameraMonitor;
import samuelpalmer.cameraautorotation.service.camera.CameraPreviewRegistrar;
import samuelpalmer.cameraautorotation.service.deviceorientation.WindowOrientationListener;
import samuelpalmer.cameraautorotation.service.facedetection.FaceDetectionResult;
import samuelpalmer.cameraautorotation.utils.AngleUtils;
import samuelpalmer.cameraautorotation.utils.FieldConstants;

public class MainAlgorithm {

    private static final String TAG = MainAlgorithm.class.getSimpleName();

    private CameraMonitor cameraMonitor;
    private ScreenOrientationController screenOrientationController;
    private Handler handler;
    private WindowOrientationListener windowOrientationListener;

    private boolean isOpen;
    private boolean isStarted;

    private int lastDeviceRotationAngle;
    private int lastDeviceRotationAngleWithUserRotation;
    private float lastUserAngle;

    public MainAlgorithm(Context context) {
        cameraMonitor = new CameraMonitor(context, userRotationListener);

        screenOrientationController = new ScreenOrientationController(context);
        screenOrientationController.start();

        handler = new Handler();
        windowOrientationListener = new WindowOrientationListener(context, handler, listener);

        isOpen = true;
    }

    public void close() {
        if (!isOpen)
            throw new IllegalStateException("Already closed");

        if (isStarted)
            stop();

        isOpen = false;

        windowOrientationListener.disable();
        windowOrientationListener = null;
        handler = null;

        screenOrientationController.stop();
        screenOrientationController = null;

        cameraMonitor.close();
        cameraMonitor = null;
    }

    public void start() {
        if (!isOpen)
            throw new IllegalStateException("Closed");

        if (isStarted)
            throw new IllegalStateException("Already started");

        lastUserAngle = -1;
        lastDeviceRotationAngleWithUserRotation = -1;
        lastDeviceRotationAngle = -1;

        windowOrientationListener.enable();

        isStarted = true;

        Log.i(TAG, "Started");
    }

    public void stop() {
        if (!isOpen)
            throw new IllegalStateException("Closed");

        if (!isStarted)
            throw new IllegalStateException("Not started");

        isStarted = false;

        windowOrientationListener.disable();

        if (cameraMonitor.isStarted())
            cameraMonitor.stop();

        CameraPreviewRegistrar.clear();

        Log.i(TAG, "Stopped");
    }

    public boolean isStarted() {
        if (!isOpen)
            throw new IllegalStateException("Closed");

        return isStarted;
    }

    private final CameraMonitor.Listener userRotationListener = new CameraMonitor.Listener() {

        @Override
        public void onUserRotationChanged(FaceDetectionResult detectionResult, Image image, Integer sensorOrientationDegrees) {
            if (isOpen && isStarted) {
                if (detectionResult.prominentFace == null) {
                    // No face detected
                    Log.v(TAG, "No face detected");
                    lastUserAngle = -1;
                } else {
                    lastUserAngle = detectionResult.prominentFace.angle;
                    Log.v(TAG, "Detected user angle: " + lastUserAngle);

                    int newScreenOrientation = Surface.ROTATION_0 + (Math.round(lastUserAngle / 90) % 4);
                    if (newScreenOrientation != screenOrientationController.getLastProposed()) {
                        Log.d(TAG, "New screen orientation: " + FieldConstants.getName(newScreenOrientation, "ROTATION_", Surface.class));
                        screenOrientationController.propose(newScreenOrientation);
                    }
                }

                // Regardless of whether we found a face, we need to record that detection has been performed for this device angle.
                // Otherwise we won't know when to perform the next detection.
                if (lastDeviceRotationAngle >= 0) {
                    Log.d(TAG, "User angle detection complete at device angle " + lastDeviceRotationAngle);

                    // TODO: Set this to the device rotation angle *at the time that the photo was taken*. The *current angle* could be different since face detection could take up to a second or two.
                    lastDeviceRotationAngleWithUserRotation = lastDeviceRotationAngle;
                    updatePredictedUserAngle();
                }

                CameraPreviewRegistrar.update(image, sensorOrientationDegrees, detectionResult, screenOrientationController.getLastProposed() == -1 ? Surface.ROTATION_0 : screenOrientationController.getLastProposed());

                // We've taken our picture, so now we'll turn off the camera to save battery power
                Log.d(TAG, "Stopping camera since picture has been taken");
                if (cameraMonitor.isStarted())
                    cameraMonitor.stop();
            }
        }

    };

    private void updatePredictedUserAngle() {
        float predictedUserAngle = predictedUserAngle();
        if (predictedUserAngle >= 0)
            cameraMonitor.setPredictedUserAngle(predictedUserAngle);
    }

    private final WindowOrientationListener.Listener listener = new WindowOrientationListener.Listener() {
        @Override
        public void onRotationAngleChanged(int rotationAngle) {
            if (isOpen && isStarted) {
                if (rotationAngle >= 0) {
                    lastDeviceRotationAngle = rotationAngle;

                    if (shouldAcquireUserRotation()) {
                        Log.d(TAG, "Device angle has changed to " + lastDeviceRotationAngle + ". Starting camera.");
                        updatePredictedUserAngle();
                        if (!cameraMonitor.isStarted())
                            cameraMonitor.start();
                    }
                }
            }
        }
    };

    private boolean shouldAcquireUserRotation() {
        // Only acquiring user rotation when necessary since the detection uses a lot of battery

        if (isOpen && isStarted && lastDeviceRotationAngle >= 0 && !cameraMonitor.isStarted()) {
            if (lastDeviceRotationAngleWithUserRotation < 0) {
                // We've never acquired the user's rotation, so we'd better do so straight away
                return true;
            } else {
                float differenceSinceLastUserRotationAcquired = Math.abs(AngleUtils.normaliseAngleDifference(lastDeviceRotationAngle - lastDeviceRotationAngleWithUserRotation));
                if (differenceSinceLastUserRotationAcquired >= 67.5) {
                    // The device's rotation has changed enough to change the screen orientation, so it's time to grab the user's rotation
                    return true;
                }
            }
        }

        return false;
    }

    private float predictedUserAngle() {
        if (lastDeviceRotationAngle < 0 || lastDeviceRotationAngleWithUserRotation < 0 || lastUserAngle < 0)
            return -1;

        return AngleUtils.normaliseAngle(lastUserAngle + (lastDeviceRotationAngle - lastDeviceRotationAngleWithUserRotation));
    }

}
