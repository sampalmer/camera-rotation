package samuelpalmer.cameraautorotation;

import android.app.PendingIntent;

public abstract class PendingIntentIds {

    public static final int MAIN_NOTIFICATION_CLICK = 1;

    // Always cancelling the existing pending intent since it's possible for the existing one to die and become permanently useless.
    public static final int PENDING_INTENT_FLAGS = PendingIntent.FLAG_CANCEL_CURRENT;

}
