package samuelpalmer.cameraautorotation.ui.preview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import java.util.Locale;

import samuelpalmer.cameraautorotation.R;
import samuelpalmer.cameraautorotation.service.facedetection.FaceDetectionFace;
import samuelpalmer.cameraautorotation.utils.AngleUtils;

/**
 * Displays annotations intended to be shown over the top of a camera image.
 * The annotations are not written into the image itself since they need to be scaled separately,
 * and the text needs to be rendered without transformation.
 */
public class CameraPreviewOverlay extends View {

    /**
     * Only use these as temporary placeholders during mapping
     */
    private final float[] mappingPoints = new float[4];

    /**
     * Only use this as a temporary placeholders during mapping
     */
    private final Rect mappingRect = new Rect();

    private final Paint boundsPaint;
    private final Paint textPaint;

    private CameraPreviewFrame frame;
    private Matrix matrix;
    private Rect clipRect;

    public CameraPreviewOverlay(Context context, AttributeSet attrs) {
        super(context, attrs);

        boundsPaint = new Paint();
        boundsPaint.setColor(Color.WHITE);
        boundsPaint.setStyle(Paint.Style.STROKE);
        boundsPaint.setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.overlayLine));

        textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(getResources().getDimensionPixelSize(R.dimen.overlayText));
        textPaint.setTextAlign(Paint.Align.CENTER);
    }

    public void setFrame(CameraPreviewFrame frame) {
        this.frame = frame;

        if (frame == null) {
            matrix = null;
            clipRect = null;
        } else {
            recalculateMatrix();
            recalculateClip();
        }

        invalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        if (frame != null) {
            recalculateMatrix();
            recalculateClip();
            invalidate();
        }
    }

    private void recalculateMatrix() {
        if (frame == null)
            throw new IllegalStateException("Cannot recalculate matrix without a frame available");

        matrix = new Matrix();

        // Centre the image first so the remaining transformations can pivot about the centre easily
        matrix.postTranslate((getWidth() - frame.image.getWidth()) / 2f, (getHeight() - frame.image.getHeight()) / 2f);

        // In order for the preview to be oriented correctly, we must factor in the user's orientation (not the display orientation)
        int rotationDegrees = frame.cameraOrientationDegrees + frame.userRotation * 90;

        // Rotate the image about the centre of the view
        matrix.postRotate(rotationDegrees, getWidth() / 2f, getHeight() / 2f);

        // Flip the image dimensions if its orientation (portrait VS landscape) changed
        RectF bitmapRotatedDimensions;
        if (rotationDegrees / 90 % 2 == 0)
            bitmapRotatedDimensions = new RectF(0, 0, frame.image.getWidth(), frame.image.getHeight());
        else
            bitmapRotatedDimensions = new RectF(0, 0, frame.image.getHeight(), frame.image.getWidth());

        // Scale the image to fit while preserving the original aspect ratio
        float scale = Math.min(
                (float) getWidth() / bitmapRotatedDimensions.width(),
                (float) getHeight() / bitmapRotatedDimensions.height()
        );

        // Apply the scale, and also horizontally flip the image so it looks to the user like they are looking in a physical mirror
        matrix.postScale(-scale, scale, getWidth() / 2f, getHeight() / 2f);
    }

    private void recalculateClip() {
        if (frame == null)
            throw new IllegalStateException("Cannot recalculate clip without a frame available");

        if (matrix == null)
            throw new IllegalStateException("Cannot recalculate clip without a matrix available");

        Rect imageSize = new Rect(0, 0, frame.image.getWidth(), frame.image.getHeight());
        mapRect(imageSize, mappingRect);
        clipRect = new Rect(mappingRect); // Making a copy to make sure mappingRect is freed up
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (frame != null) {
            // Don't want the annotations to overflow beyond the image into the empty space around it.
            // Eg, for a portrait image, a face rectangle could overflow into the left or right side of the image.
            canvas.clipRect(clipRect);

            canvas.drawBitmap(frame.image, matrix, null);

            for (FaceDetectionFace face : frame.detectionResult.allFaces) {
                // Draw bounds
                mapRect(face.bounds, mappingRect);
                canvas.drawRect(mappingRect, boundsPaint);

                // Draw angle in the middle
                float angleInteger = AngleUtils.normaliseAngle(Math.round(face.angle)); // Need to re-normalise after rounding since the angle might have been rounded up out of range (eg to 360)
                String formattedAngle = String.format(Locale.getDefault(), "%.0f°", angleInteger);
                drawCenteredText(canvas, formattedAngle, mappingRect.exactCenterX(), mappingRect.exactCenterY(), textPaint);

                // Draw dimensions at the top of the box
                String formattedDimensions = String.format(Locale.getDefault(), "%d×%d px", face.bounds.width(), face.bounds.height());
                drawCenteredText(canvas, formattedDimensions, mappingRect.exactCenterX(), mappingRect.exactCenterY() + textPaint.getTextSize() * 2, textPaint);
            }
        }
    }

    private static void drawCenteredText(Canvas canvas, String formattedAngle, float centerX, float centerY, Paint paint) {
        if (paint.getTextAlign() != Paint.Align.CENTER)
            throw new IllegalArgumentException("Paint text alignment must be horizontally-centered");

        // The Y coordinate in drawText() is for the text's baseline rather than the text's centre.
        // So we need to calculate a baseline position that results in the text being vertically centered.
        // Taken from https://stackoverflow.com/a/10255500/238753
        float baseLineY = centerY - (paint.descent() + paint.ascent()) / 2f;

        canvas.drawText(formattedAngle, centerX, baseLineY, paint);
    }

    private void mapRect(Rect unmapped, Rect mapped) {
        mappingPoints[0] = unmapped.left;
        mappingPoints[1] = unmapped.top;
        mappingPoints[2] = unmapped.right;
        mappingPoints[3] = unmapped.bottom;

        matrix.mapPoints(mappingPoints);

        // The points must be properly sorted, otherwise some things, such as clipping, won't work
        mapped.set(
                Math.round(Math.min(mappingPoints[0], mappingPoints[2])),
                Math.round(Math.min(mappingPoints[1], mappingPoints[3])),
                Math.round(Math.max(mappingPoints[0], mappingPoints[2])),
                Math.round(Math.max(mappingPoints[1], mappingPoints[3]))
        );
    }

}
