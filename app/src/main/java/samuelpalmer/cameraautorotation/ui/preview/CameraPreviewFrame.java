package samuelpalmer.cameraautorotation.ui.preview;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;

import samuelpalmer.cameraautorotation.service.facedetection.FaceDetectionResult;
import samuelpalmer.cameraautorotation.utils.BitmapUtils;

class CameraPreviewFrame {

    public final Bitmap image;
    public final int cameraOrientationDegrees;
    public final FaceDetectionResult detectionResult;
    public final int userRotation;

    public CameraPreviewFrame(Context context, Image image, int cameraOrientationDegrees, FaceDetectionResult detectionResult, int userRotation) {
        this.image = BitmapUtils.YUV_420_888_toRGBIntrinsics(context, image);
        this.cameraOrientationDegrees = cameraOrientationDegrees;
        this.detectionResult = detectionResult;
        this.userRotation = userRotation;
    }

}
