package samuelpalmer.cameraautorotation.ui.preview;

import android.app.Application;
import android.media.Image;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import samuelpalmer.cameraautorotation.service.camera.CameraPreviewRegistrar;
import samuelpalmer.cameraautorotation.service.facedetection.FaceDetectionResult;

public class CameraPreviewViewModel extends AndroidViewModel {

    private final MutableLiveData<CameraPreviewFrame> frame;

    public CameraPreviewViewModel(@NonNull Application application) {
        super(application);
        frame = new MutableLiveData<>();

        CameraPreviewRegistrar.addPreview(listener);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        CameraPreviewRegistrar.removePreview(listener);
    }

    public LiveData<CameraPreviewFrame> getFrame() {
        return frame;
    }

    private final CameraPreviewRegistrar.Listener listener = new CameraPreviewRegistrar.Listener() {

        @Override
        public void onPreviewAvailable(Image image, int cameraOrientationDegrees, FaceDetectionResult detectionResult, int userRotation) {
            if (frame.hasActiveObservers()) {
                // Converting the image to a bitmap is expensive, so we'll only keep the frame if someone's listening
                CameraPreviewFrame newFrame = new CameraPreviewFrame(getApplication(), image, cameraOrientationDegrees, detectionResult, userRotation);
                frame.setValue(newFrame);
            } else {
                // The frame that we were keeping track of is now out-of-date, so we'd better clear it to reflect that to the view
                frame.setValue(null);
            }
        }

        @Override
        public void onPreviewUnavailable() {
            frame.setValue(null);
        }

    };

}
