package samuelpalmer.cameraautorotation.ui.preview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import samuelpalmer.cameraautorotation.R;

public class CameraPreviewFragment extends Fragment {

    private static final String TAG = CameraPreviewFragment.class.getSimpleName();

    private CameraPreviewViewModel model;
    private CameraPreviewOverlay overlay;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = ViewModelProviders.of(this).get(CameraPreviewViewModel.class);
        model.getFrame().observe(this, frameObserver);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_camera_preview, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        overlay = view.findViewById(R.id.camera_preview_overlay);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        overlay = null;
    }

    private final Observer<CameraPreviewFrame> frameObserver = new Observer<CameraPreviewFrame>() {
        @Override
        public void onChanged(CameraPreviewFrame frame) {
            overlay.setFrame(frame);
        }
    };

}
