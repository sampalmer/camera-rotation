package samuelpalmer.cameraautorotation.ui;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.acra.ACRA;

import samuelpalmer.cameraautorotation.ApplicationSettings;
import samuelpalmer.cameraautorotation.R;
import samuelpalmer.cameraautorotation.service.ServiceController;
import samuelpalmer.cameraautorotation.utils.SwitchBar;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int REQUEST_CAMERA = 1;
    private static final int REQUEST_SYSTEM_SETTINGS = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences prefs = ApplicationSettings.getSharedPreferences(this);
        boolean isOn = prefs.getBoolean(ApplicationSettings.KEY_IS_ON, false);

        SwitchBar mainSwitch = findViewById(R.id.activity_main_switch);
        mainSwitch.setChecked(isOn);
        mainSwitch.setOnSwitchChangeListener((switchView, isChecked) -> {
            boolean isNowOn = mainSwitch.isChecked();

            if (isNowOn) {
                prefs.edit().putBoolean(ApplicationSettings.KEY_IS_ON, true).apply();
                ServiceController.startService(MainActivity.this);
            } else {
                prefs.edit().putBoolean(ApplicationSettings.KEY_IS_ON, false).apply();
                ServiceController.stopService(MainActivity.this);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        MenuItem bugReportItem = menu.findItem(R.id.menu_report_bug);
        bugReportItem.setOnMenuItemClickListener(item -> {
            ACRA.getErrorReporter().handleException(new RuntimeException("User-reported error"), false);
            return true;
        });

        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{ Manifest.permission.CAMERA }, REQUEST_CAMERA);
        else
            checkSystemSettingsPermission();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CAMERA) {
            if (grantResults.length <= 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED)
                finish();
            else
                checkSystemSettingsPermission();
        }
    }

    private void checkSystemSettingsPermission() {
        if (!Settings.System.canWrite(this)) {
            Intent systemSettingsIntent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.fromParts("package", getPackageName(), null));
            startActivityForResult(systemSettingsIntent, REQUEST_SYSTEM_SETTINGS);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_SYSTEM_SETTINGS)
            if (!Settings.System.canWrite(this))
                finish();
    }
}
