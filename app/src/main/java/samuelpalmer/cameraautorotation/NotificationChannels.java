package samuelpalmer.cameraautorotation;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

public abstract class NotificationChannels {

    public static final String ID_SERVICE = "service";

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static void setup(Context context) {
        NotificationChannel controls = new NotificationChannel(
                ID_SERVICE,
                context.getText(R.string.notification_channel_service),

                // This should be as low as possible since the notification is effectively permanent and takes up space in the notification shade.
                // IMPORTANCE_NONE and IMPORTANCE_MIN can't be used because they cause the OS "Running in background" notification to show up.
                NotificationManager.IMPORTANCE_LOW
        );
        controls.setShowBadge(false); // This isn't needed, but it makes it clearer to the user that the service notification won't show as a badge.
        controls.setLockscreenVisibility(Notification.VISIBILITY_SECRET); // So it won't show on the lock screen since that's not really useful

        NotificationManager notificationManager = ContextCompat.getSystemService(context, NotificationManager.class);
        assert notificationManager != null;
        notificationManager.createNotificationChannel(controls);
    }


}
