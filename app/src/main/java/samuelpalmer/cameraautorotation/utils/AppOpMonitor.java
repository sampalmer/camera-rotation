package samuelpalmer.cameraautorotation.utils;

import android.app.AppOpsManager;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Process;

import androidx.annotation.RequiresApi;

import java.util.HashSet;
import java.util.Set;

/**
 * Monitors changes to a particular app op.
 * @see AppOpsManager
 */
@RequiresApi(api = Build.VERSION_CODES.KITKAT)
public class AppOpMonitor {

    private final AppOpsManager appOps;
    private final int uid;
    private final String packageName;
    private final Handler handler;
    private final Set<Listener> listeners;
    private final String op;

    private boolean isUpToDate = false;
    private boolean hadPermission;

    public interface Listener {
        void onOpChanged(String op);
    }

    /**
     * @param op The <code>OPSTR_</code> constant from {@link AppOpsManager} for the op you want to monitor
     */
    public AppOpMonitor(Context context, String op) {
        // Not using ContextCompat.getSystemService() because it doesn't work on older versions of Android (as far as I recall)
        appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        uid = Process.myUid();
        packageName = context.getPackageName();
        handler = new Handler();
        listeners = new HashSet<>();
        this.op = op;
    }

    public boolean hasPermission() {
        if (isUpToDate)
            return hadPermission;
        else
            return poll();
    }

    public void subscribe(Listener listener) {
        boolean added = listeners.add(listener);
        if (!added)
            throw new IllegalArgumentException("Listener already subscribed");

        boolean isFirstListener = listeners.size() == 1;
        if (isFirstListener)
            appOps.startWatchingMode(op, packageName, usageOpListener);
    }

    public void unsubscribe(Listener listener) {
        boolean removed = listeners.remove(listener);
        if (!removed)
            throw new IllegalStateException("Listener was not subscribed");

        if (listeners.isEmpty()) {
            appOps.stopWatchingMode(usageOpListener);
            handler.removeCallbacks(checkUsagePermission);
            isUpToDate = false;
        }
    }

    private final AppOpsManager.OnOpChangedListener usageOpListener = new AppOpsManager.OnOpChangedListener() {
        @Override
        public void onOpChanged(String op, String packageName) {
            if (AppOpMonitor.this.op.equals(op)) {
                // We get callbacks for op changes across *all* apps
                if (AppOpMonitor.this.packageName.equals(packageName)) {
                    // Not in main thread, so post to main thread queue
                    handler.post(checkUsagePermission);
                }
            }
        }
    };

    private final Runnable checkUsagePermission = new Runnable() {
        @Override
        public void run() {
            if (!listeners.isEmpty()) {
                boolean hasPermission = poll();
                if (!isUpToDate || hasPermission != hadPermission) {
                    hadPermission = hasPermission;
                    isUpToDate = true;

                    for (Listener listener : listeners)
                        listener.onOpChanged(op);
                }
            }
        }
    };

    private boolean poll() {
        int mode = appOps.checkOpNoThrow(op, uid, packageName);
        return mode == AppOpsManager.MODE_ALLOWED;
    }

}
