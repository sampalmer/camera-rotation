package samuelpalmer.cameraautorotation.utils;

/**
 * Compares timestamps in nanoseconds. Takes into account the possibility of number overflow.
 */
public abstract class TimestampComparer {

    public static int compare(long aNanos, long bNanos) {
        long difference = aNanos - bNanos;

        if (difference < 0)
            return -1;
        else if (difference > 0)
            return 1;
        else
            return 0;
    }

}
