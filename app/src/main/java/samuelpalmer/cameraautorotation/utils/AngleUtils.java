package samuelpalmer.cameraautorotation.utils;

public abstract class AngleUtils {

    /**
     * Converts the given angle so that it is in the range [0, 360)
     */
    public static float normaliseAngle(float angle) {
        if (Float.isNaN(angle) || Float.isInfinite(angle))
            throw new IllegalArgumentException("Angle is not finite");

        while (angle < 0)
            angle += (float) 360;

        return angle % (float) 360;
    }

    /**
     * Converts the given difference between angles so that it is in the range [0, 180]
     */
    public static float normaliseAngleDifference(float angleDifference) {
        float zeroTo360 = normaliseAngle(angleDifference);
        if (zeroTo360 > 180)
            return 360 - zeroTo360;
        else
            return zeroTo360;
    }

}
