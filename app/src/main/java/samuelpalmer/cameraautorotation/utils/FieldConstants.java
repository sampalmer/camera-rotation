package samuelpalmer.cameraautorotation.utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * Makes constant static fields nicer to work with
 */
@SuppressWarnings("unused")
public abstract class FieldConstants {

    /**
     * Shorthand for {@link #getName(Object, String, Class[])} but without a <code>prefix</code>.
     */
    @NonNull
    public static String getName(@Nullable Object value, @NonNull Class... containingClasses) {
        return getName(value, null, containingClasses);
    }

    /**
     * Gets the name of the constant field with the given value
     * @param value The constant whose name should be retrieved
     * @param prefix Limits the searched constants to only include those whose names start with this value
     * @param containingClasses The classes whose declared fields will be included in the search
     */
    @NonNull
    public static String getName(@Nullable Object value, @Nullable String prefix, @NonNull Class... containingClasses) {
        if (containingClasses.length <= 0)
            throw new IllegalArgumentException("No classes supplied");

        String result = null;

        for (Class containingClass : containingClasses)
            for (Field field : containingClass.getDeclaredFields()) {
                String fieldName = field.getName();
                boolean matchesPrefix = prefix == null || fieldName.startsWith(prefix);

                int modifiers = field.getModifiers();
                boolean isPublic = Modifier.isPublic(modifiers);
                boolean isStatic = Modifier.isStatic(modifiers);

                if (matchesPrefix && isStatic && isPublic) {
                    Object currentFieldValue;
                    try {
                        currentFieldValue = field.get(null);
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }

                    if (currentFieldValue.equals(value)) {
                        if (result != null && !result.equals(fieldName) /* eg PixelFormat.JPEG == ImageFormat.JPEG */) {
                            throw new IllegalArgumentException("Value " + value + " matches fields " + result + " and " + fieldName);
                        } else {
                            result = fieldName;
                        }
                    }
                }
            }

        if (result == null)
            throw new RuntimeException("Couldn't find value " + value + " in class " + containingClasses[0].getSimpleName());

        return result;
    }

}
