package samuelpalmer.cameraautorotation.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;

public class DeviceInteractivityMonitor  {
    
    private final Context context;
    private final Listener listener;

    private boolean isUpToDate = false;
    private boolean started;
    private boolean last;

    public interface Listener {
        void onInteractivityChanged();
    }

    public DeviceInteractivityMonitor(Context context, Listener listener) {
        this.context = context;
        this.listener = listener;
    }
    
    public void start() {
        if (started)
            throw new IllegalStateException("Already started");

        context.registerReceiver(screenOnReceiver, new IntentFilter(Intent.ACTION_SCREEN_ON));
        context.registerReceiver(screenOffReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));

        started = true;
    }
    
    public void stop() {
        if (!started)
            throw new IllegalStateException("Already stopped");

        context.unregisterReceiver(screenOffReceiver);
        context.unregisterReceiver(screenOnReceiver);
        isUpToDate = false; // We're no longer subscribed, so the last received value might be out-of-date.

        started = false;
    }
    
    public boolean isInteractive() {
        if (isUpToDate)
            return last;
        else
            return poll();
    }
    
    private boolean poll() {
        PowerManager powerManager = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
        return powerManager.isInteractive();
    }

    private final BroadcastReceiver screenOnReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (started) {
                last = true;
                isUpToDate = true;
                listener.onInteractivityChanged();
            }
        }
    };

    private final BroadcastReceiver screenOffReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (started) {
                last = false;
                isUpToDate = true;
                listener.onInteractivityChanged();
            }
        }
    };

}
