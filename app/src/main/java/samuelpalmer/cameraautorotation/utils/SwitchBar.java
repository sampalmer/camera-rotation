package samuelpalmer.cameraautorotation.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import samuelpalmer.cameraautorotation.R;

public class SwitchBar extends RelativeLayout {

    public interface OnSwitchChangeListener {
        /**
         * Called when the checked state of the Switch has changed.
         *
         * @param switchView The Switch view whose state has changed.
         * @param isChecked  The new checked state of switchView.
         */
        void onSwitchChanged(Switch switchView, boolean isChecked);
    }

    private final Toolbar toolbar;
    private final TextView textView;
    private final Switch switchView;
    private OnSwitchChangeListener onSwitchChangeListener;
	
    public SwitchBar(Context context, AttributeSet attrs) {
        super(context, attrs);

	    LayoutInflater.from(context).inflate(R.layout.view_switchbar, this);

        toolbar = findViewById(R.id.view_switchbar_toolbar);
        textView = findViewById(R.id.view_switchbar_text);
        switchView = findViewById(R.id.view_switchbar_switch);

	    int switchBarBackgroundColor = ContextCompat.getColor(context, R.color.colorSwitchBar);
	    setBackgroundColor(switchBarBackgroundColor);
	    switchView.setBackgroundColor(switchBarBackgroundColor);

	    updateText(switchView.isChecked());
	    switchView.setOnCheckedChangeListener((buttonView, isChecked) -> {
            updateText(isChecked);

            if (onSwitchChangeListener != null)
                onSwitchChangeListener.onSwitchChanged(switchView, isChecked);
        });

        toolbar.setOnClickListener(v -> {
            boolean isChecked = !switchView.isChecked();
            setChecked(isChecked);
        });
    }

    private void updateText(boolean isChecked) {
        textView.setText(isChecked ? R.string.on : R.string.off);
    }

    public void setChecked(boolean checked) {
        switchView.setChecked(checked);
    }

    public boolean isChecked() {
        return switchView.isChecked();
    }
    
    public void setOnSwitchChangeListener(OnSwitchChangeListener listener) {
        onSwitchChangeListener = listener;
    }

}
