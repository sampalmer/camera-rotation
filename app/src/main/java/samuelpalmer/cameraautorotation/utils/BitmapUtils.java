package samuelpalmer.cameraautorotation.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.media.Image;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicYuvToRGB;

import java.nio.ByteBuffer;

public abstract class BitmapUtils {

    // Taken from https://stackoverflow.com/q/44652828/238753
    public static Bitmap YUV_420_888_toRGBIntrinsics(Context context, Image image) {
        if (image == null)
            throw new IllegalArgumentException();
        if (image.getFormat() != ImageFormat.YUV_420_888)
            throw new IllegalArgumentException("Image is in unsupported format: " + FieldConstants.getName(image.getFormat(), ImageFormat.class));

        final ByteBuffer yuvBytes = imageToByteBuffer(image);

        // Convert YUV to RGB

        final RenderScript rs = RenderScript.create(context);

        final Bitmap        bitmap     = Bitmap.createBitmap(image.getWidth(), image.getHeight(), Bitmap.Config.ARGB_8888);
        final Allocation allocationRgb = Allocation.createFromBitmap(rs, bitmap);

        final Allocation allocationYuv = Allocation.createSized(rs, Element.U8(rs), yuvBytes.array().length);
        allocationYuv.copyFrom(yuvBytes.array());

        ScriptIntrinsicYuvToRGB scriptYuvToRgb = ScriptIntrinsicYuvToRGB.create(rs, Element.U8_4(rs));
        scriptYuvToRgb.setInput(allocationYuv);
        scriptYuvToRgb.forEach(allocationRgb);

        allocationRgb.copyTo(bitmap);

        // Release

        allocationYuv.destroy();
        allocationRgb.destroy();
        rs.destroy();

        return bitmap;
    }

    // Taken from https://stackoverflow.com/a/47601824/238753
    private static ByteBuffer imageToByteBuffer(final Image image)
    {
        final Rect crop   = image.getCropRect();
        final int  width  = crop.width();
        final int  height = crop.height();

        final Image.Plane[] planes     = image.getPlanes();
        final byte[]        rowData    = new byte[planes[0].getRowStride()];
        final int           bufferSize = width * height * ImageFormat.getBitsPerPixel(ImageFormat.YUV_420_888) / 8;
        final ByteBuffer    output     = ByteBuffer.allocateDirect(bufferSize);

        int channelOffset = 0;
        int outputStride = 0;

        for (int planeIndex = 0; planeIndex < 3; planeIndex++)
        {
            if (planeIndex == 0)
            {
                channelOffset = 0;
                outputStride = 1;
            }
            else if (planeIndex == 1)
            {
                channelOffset = width * height + 1;
                outputStride = 2;
            }
            else if (planeIndex == 2)
            {
                channelOffset = width * height;
                outputStride = 2;
            }

            final ByteBuffer buffer      = planes[planeIndex].getBuffer();
            final int        rowStride   = planes[planeIndex].getRowStride();
            final int        pixelStride = planes[planeIndex].getPixelStride();

            final int shift         = (planeIndex == 0) ? 0 : 1;
            final int widthShifted  = width >> shift;
            final int heightShifted = height >> shift;

            buffer.position(rowStride * (crop.top >> shift) + pixelStride * (crop.left >> shift));

            for (int row = 0; row < heightShifted; row++)
            {
                final int length;

                if (pixelStride == 1 && outputStride == 1)
                {
                    length = widthShifted;
                    buffer.get(output.array(), channelOffset, length);
                    channelOffset += length;
                }
                else
                {
                    length = (widthShifted - 1) * pixelStride + 1;
                    buffer.get(rowData, 0, length);

                    for (int col = 0; col < widthShifted; col++)
                    {
                        output.array()[channelOffset] = rowData[col * pixelStride];
                        channelOffset += outputStride;
                    }
                }

                if (row < heightShifted - 1)
                {
                    buffer.position(buffer.position() + rowStride - length);
                }
            }

            buffer.clear(); // Resetting the buffer position so it's ready for the next consumer
        }

        return output;
    }

    public static void autoLevel(Image image, float autoLevelAmount) {
        if (image == null)
            throw new IllegalArgumentException();
        if (image.getFormat() != ImageFormat.YUV_420_888)
            throw new IllegalArgumentException("Image is in unsupported format: " + FieldConstants.getName(image.getFormat(), ImageFormat.class));

        final Rect crop   = image.getCropRect();
        final int  width  = crop.width();
        final int  height = crop.height();

        Image.Plane yPlane = image.getPlanes()[0];
        final byte[]        rowData    = new byte[yPlane.getRowStride()];

        final ByteBuffer buffer      = yPlane.getBuffer();
        final int        rowStride   = yPlane.getRowStride();

        // First measure the dynamic range
        int lowLevel = 255;
        int highLevel = 0;

        buffer.position(rowStride * (crop.top) + (crop.left));
        for (int row = 0; row < height; row++)
        {
            buffer.get(rowData, 0, width);

            for (int col = 0; col < width; col++)
            {
                int luminance = rowData[col] & 0xFF; // Changes the range from [-128, 127] to [0, 255];
                lowLevel = Math.min(lowLevel, luminance);
                highLevel = Math.max(highLevel, luminance);
            }

            if (row < height - 1)
            {
                buffer.position(buffer.position() + rowStride - width);
            }
        }

        // Then rewrite the plane using the new range
        int dynamicRange = highLevel - lowLevel;
        if (dynamicRange > 0) { // If there's no range, we would get a divide-by-zero
            buffer.position(rowStride * (crop.top) + (crop.left));
            for (int row = 0; row < height; row++)
            {
                int rowStartPosition = buffer.position();
                buffer.get(rowData, 0, width);

                for (int col = 0; col < width; col++)
                {
                    int originalLuminance = rowData[col] & 0xFF; // Changes the range from [-128, 127] to [0, 255];
                    int leveledLuminance = (originalLuminance - lowLevel) * 255 / dynamicRange;
                    int mixedLeveledLuminance = (int) (leveledLuminance * autoLevelAmount + originalLuminance * (1.0f - autoLevelAmount));
                    rowData[col] = (byte) mixedLeveledLuminance;
                }

                buffer.position(rowStartPosition);
                buffer.put(rowData, 0, width);

                if (row < height - 1)
                {
                    buffer.position(buffer.position() + rowStride - width);
                }
            }
        }

        buffer.clear();
    }

}
