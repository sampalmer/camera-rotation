package samuelpalmer.cameraautorotation;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import org.acra.ACRA;
import org.acra.annotation.AcraCore;
import org.acra.annotation.AcraMailSender;
import org.acra.annotation.AcraNotification;
import org.acra.data.StringFormat;

@AcraCore(
        buildConfigClass = BuildConfig.class,
        logcatFilterByPid = false,
        logcatArguments = { "-d", "-v", "threadtime" },
        reportFormat = StringFormat.KEY_VALUE_LIST, // The logcat output is much easier to read this way
        alsoReportToAndroidFramework = true
)
@AcraNotification(
        resChannelName = R.string.notification_channel_errors,
        resTitle = R.string.notification_error_title,
        resText = R.string.notification_error_text,
        resSendButtonText = R.string.error_report_button_send,
        resSendWithCommentButtonText = R.string.error_report_button_send_comments,
        resDiscardButtonText = R.string.error_report_button_dont_send
)
@AcraMailSender(
        mailTo = "Samuel Palmer", // I don't want my email address to end up in source control
        resSubject = R.string.error_report_email_subject,
        reportFileName = "error.txt"
)
public class CurrentProcess extends Application {

    private static final String TAG = CurrentProcess.class.getSimpleName();

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        // The following line triggers the initialization of ACRA
        ACRA.init(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "Process created");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.i(TAG, "Configuring notification channels");

            // Always setting up channels on startup since the OS notification channel settings can get out of sync with the app settings.
            // Eg the OS might backup and restore app settings
            NotificationChannels.setup(this);
        }
    }

}
