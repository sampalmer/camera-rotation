package samuelpalmer.cameraautorotation;

import android.content.Context;
import android.content.SharedPreferences;

public abstract class ApplicationSettings {

    /**
     * Whether the application is turned on.
     */
    public static final String KEY_IS_ON = "isOn";

    public static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
    }

}
